# coding: utf-8

from django.shortcuts import render

from django.contrib.messages.views import SuccessMessageMixin
from django.utils import timezone
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView

from braces.views import LoginRequiredMixin

from .forms import UTInscripcionesForm
from inscripciones.models import Inscripcion
from calificaciones.models import FechasCapturacion, Calificacion
from grupos.models import Grupo
from profesores.models import Imparte
from alumnos.models import Alumnos


class UTInscripcionesCreationView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'inscripciones/master.jade'
    form_class = UTInscripcionesForm
    model = Inscripcion
    success_url = reverse_lazy('register_inscripcion')
    success_message = 'Sea registrado con esxito %(alumno)s '

    def post(self, *args, **kwargs):
        response = super(UTInscripcionesCreationView, self).post(*args, **kwargs)
        try:
            alumno = Alumnos.objects.get(id=self.request.POST.get('alumno'))
            clave_grupo = Grupo.objects.get(id=self.request.POST.get('clave_grupo'))
            inscripcion = Inscripcion.objects.get(alumno=alumno, clave_grupo=clave_grupo)
            imparte = Imparte.objects.get(clave_grupo=clave_grupo)
            Calificacion.objects.create(alumno=alumno, imparte=imparte)
        except:
            pass
        return response


class UTIncripcionesEditView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Inscripcion
    fields = '__all__'
    success_url = reverse_lazy('listar_inscripciones')
    template_name = 'inscripciones/master.jade'
    success_message = 'Actualizando la inscripcion del alumn %(alumno)s'


class UTInscripcionesListView(LoginRequiredMixin, ListView):
    model = Inscripcion
    template_name = 'inscripciones/list.jade'
    context_object_name = 'lista_incripcion'

    def get_context_data(self, *args, **kwargs):
        context = super(UTInscripcionesListView, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        if pk:
            try:
                imparte_obj = Imparte.objects.get(pk=pk)
                context['imparte_obj'] = imparte_obj
            except:
                pass

        context['disabled_fields'] = self.get_disabled_fields()
        return context

    def get_disabled_fields(self):
        fields = []
        hoy = timezone.now().date()
        fechas = FechasCapturacion.objects.get(active=True)
        if not (hoy > fechas.fecha_p1i and hoy < fechas.fecha_p1f):
            fields.append('parcial_1')
        if not (hoy > fechas.fecha_p2i and  hoy < fechas.fecha_p2f):
            fields.append('parcial_2')
        if not (hoy > fechas.fecha_p3i and  hoy < fechas.fecha_p3f):
            fields.append('parcial_3')
        if not (hoy > fechas.fecha_r3i and  hoy < fechas.fecha_r3f):
            fields.append('remedial_3')
        if not (hoy > fechas.fecha_r2i and  hoy < fechas.fecha_r2f):
            fields.append('remedial_2')
        if not (hoy > fechas.fecha_r1i and  hoy < fechas.fecha_r1f):
            fields.append('remedial_1')
        if not (hoy > fechas.fecha_ei and  hoy < fechas.fecha_ef):
            fields.append('extra')

        return fields

    def get_queryset(self):
        qs = super(UTInscripcionesListView, self).get_queryset()
        pk = self.kwargs.get('pk')
        if pk:
            try:
                imparte_obj = Imparte.objects.get(pk=pk)
                return qs.filter(clave_grupo=imparte_obj.clave_grupo)
            except:
                return qs.none()

        return qs