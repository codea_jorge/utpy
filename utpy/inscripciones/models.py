# -*- coding: utf-8 -*-

from django.db import models

from alumnos.models import Alumnos
from grupos.models import Grupo
from modalidades.models import Modalidad


class Inscripcion(models.Model): 
    alumno = models.ForeignKey(Alumnos)
    clave_grupo = models.ForeignKey(Grupo)
    modalidad = models.ForeignKey(Modalidad)
    fecha_inscripcion = models.DateField()

    class Meta:
        # Evitar registrar un alumno en dos grupos
        unique_together = ('alumno', 'clave_grupo')

    def __unicode__(self):
        return self.alumno.__unicode__() + ' -- ' + self.clave_grupo.__unicode__()
