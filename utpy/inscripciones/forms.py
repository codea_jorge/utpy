# -*- coding: utf-8 -*-

from django import forms

from inscripciones.models import Inscripcion
from material.base import Layout, Fieldset, Row, Span8, Span4

class UTInscripcionesForm(forms.ModelForm):

    class Meta:
        model = Inscripcion
        exclude = ()

    layout = Layout(
        Fieldset('', 'alumno'),
        'clave_grupo', 'fecha_inscripcion',

    )