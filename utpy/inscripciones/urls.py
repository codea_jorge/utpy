# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^new/$', UTInscripcionesCreationView.as_view(), name='register_inscripcion'),
    url(r'^edit/(?P<pk>\d+)/$', UTIncripcionesEditView.as_view(), name='editar_inscripcion'),
    url(r'^list/$', UTInscripcionesListView.as_view(), name='listar_inscripciones'),
    url(r'^list/(?P<pk>\d+)/$', UTInscripcionesListView.as_view(), name='custom_listar_inscripciones'),
]
