# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modalidades', '0002_auto_20151207_1734'),
        ('alumnos', '0002_auto_20151208_0605'),
        ('grupos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Inscripcion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_inscripcion', models.DateField()),
                ('alumno', models.ForeignKey(to='alumnos.Alumnos')),
                ('clave_grupo', models.ForeignKey(to='grupos.Grupo')),
                ('modalidad', models.ForeignKey(to='modalidades.Modalidad')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='inscripcion',
            unique_together=set([('alumno', 'clave_grupo')]),
        ),
    ]
