# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alumnos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Documentacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('acta_nac', models.BooleanField()),
                ('curp', models.BooleanField()),
                ('certificado_bach', models.BooleanField()),
                ('compro_domicilio', models.BooleanField()),
                ('fecha_entrega', models.DateField()),
                ('fecha_regreso_doc', models.DateField()),
                ('alumno', models.ForeignKey(to='alumnos.Alumnos')),
            ],
        ),
    ]
