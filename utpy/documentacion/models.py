from django.db import models

from alumnos.models import Alumnos


class Documentacion(models.Model):
    alumno = models.ForeignKey(Alumnos)
    acta_nac = models.BooleanField()
    curp = models.BooleanField()
    certificado_bach = models.BooleanField()
    compro_domicilio = models.BooleanField()
    fecha_entrega = models.DateField()
    fecha_regreso_doc = models.DateField()
