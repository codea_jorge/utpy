# coding: utf-8

from django.views.generic import TemplateView

from braces.views import LoginRequiredMixin


class AdminIndexView(LoginRequiredMixin, TemplateView):
    template_name='index_admin.jade'
