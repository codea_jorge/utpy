# coding: utf-8

from django import template

register = template.Library()


@register.assignment_tag(takes_context=True)
def related_calif(context, alumno):
    return alumno.calificacion_set.get(imparte=context.get('imparte_obj'))
