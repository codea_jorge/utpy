# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carreras', '0001_initial'),
        ('periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clave_grupo', models.CharField(max_length=12)),
                ('cuatrimestre', models.IntegerField()),
                ('letra', models.CharField(max_length=1)),
                ('turno', models.CharField(max_length=1, choices=[(b'V', b'Vespertino'), (b'M', b'Matutino')])),
                ('clave_carrera', models.ForeignKey(to='carreras.Carrera')),
                ('clave_periodo', models.ForeignKey(to='periodos.Periodo')),
            ],
        ),
    ]
