# -*- coding: utf-8 -*-

from functools import reduce

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from braces.views import LoginRequiredMixin

from grupos.forms import UTGruposForm
from grupos.models import Grupo


class UTGruposCreationView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'grupos/master.jade'
    form_class = UTGruposForm
    model = Grupo
    success_url = reverse_lazy('register_grupo')
    success_message = 'Grupo %(letra)s creado'


class UTGruposEditView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Grupo
    fields = '__all__'
    success_url = reverse_lazy('listar_grupo')
    template_name = 'grupos/master.jade'
    success_message = 'Grupo %(letra)s con clave %(clave_grupo)s Actualizado'


class UTGruposListView(LoginRequiredMixin, ListView):
    model = Grupo
    template_name = 'grupos/list.jade'
    context_object_name = 'lista_grupo'


class UTGrupoDeleView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Grupo
    fields = '__all__'
    success_url = reverse_lazy('listar_grupo')
    template_name = 'grupos/elim.jade'
    success_message = 'Grupo %(letra)s con clave %(clave_grupo)s Eliminado'