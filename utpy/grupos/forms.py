# -*- coding: utf-8 -*-

from django import forms

from grupos.models import Grupo
from material.base import Layout, Fieldset, Row, Span8, Span4

class UTGruposForm(forms.ModelForm):
    class Meta:
        model = Grupo
        exclude = ()

    layout = Layout(
        Fieldset('', 'clave_grupo', 'clave_carrera'),
        'cuatrimestre', 'letra', 'turno', 'clave_periodo',

    )

    