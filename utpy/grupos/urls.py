# -*- conding: utf-8 -*-

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^new/$', UTGruposCreationView.as_view(), name='register_grupo'),
    url(r'^edit/(?P<pk>\d+)/$', UTGruposEditView.as_view(), name='editar_grupo'),
    url(r'^list/$', UTGruposListView.as_view(), name='listar_grupo'),
    url(r'^dele/(?P<pk>\d+)/$', UTGrupoDeleView.as_view(), name='eliminar_grupo'),
]