# -*- coding: utf-8 -*-

from django.db import models

from carreras.models import Carrera
from periodos.models import Periodo

class Grupo(models.Model):
    TURNOS = (
        ('V', 'Vespertino'),
        ('M', 'Matutino')
    )
    clave_grupo = models.CharField(max_length=12)
    clave_carrera = models.ForeignKey(Carrera)
    cuatrimestre = models.IntegerField()
    letra = models.CharField(max_length=1)
    turno = models.CharField(max_length=1, choices=TURNOS)
    clave_periodo = models.ForeignKey(Periodo)

    def __unicode__(self):
        return self.clave_grupo + ' - ' + self.clave_carrera.clave_carrera
