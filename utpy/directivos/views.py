# -*- coding: utf-8 -*-

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView

from braces.views import LoginRequiredMixin

from directivos.forms import UTDirectivosForm


class UTDirectivosCreationView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'directivos/new.jade'
    form_class = UTDirectivosForm
    success_url = reverse_lazy('register_directivo')
    success_message = 'Directivo %(nombre)s creado'
