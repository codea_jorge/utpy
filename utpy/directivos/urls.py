# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import UTDirectivosCreationView

urlpatterns = [
    url(r'^new/$', UTDirectivosCreationView.as_view(), name='register_directivo'),
]