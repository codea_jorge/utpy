# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('directivos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directivos',
            name='amaterno',
            field=models.CharField(max_length=20, verbose_name=b'Apellido Materno'),
        ),
        migrations.AlterField(
            model_name='directivos',
            name='apaterno',
            field=models.CharField(max_length=20, verbose_name=b'Apellido Paterno'),
        ),
        migrations.AlterField(
            model_name='directivos',
            name='clave',
            field=models.CharField(max_length=11, serialize=False, verbose_name=b'Clave Directivo', primary_key=True),
        ),
        migrations.AlterField(
            model_name='directivos',
            name='nombre',
            field=models.CharField(max_length=20, verbose_name=b'Nombre'),
        ),
        migrations.AlterField(
            model_name='directivos',
            name='puesto',
            field=models.CharField(max_length=30, verbose_name=b'Puesto'),
        ),
    ]
