# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Directivos',
            fields=[
                ('clave', models.CharField(max_length=11, serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=20)),
                ('apaterno', models.CharField(max_length=20)),
                ('amaterno', models.CharField(max_length=20)),
                ('puesto', models.CharField(max_length=30)),
            ],
        ),
    ]
