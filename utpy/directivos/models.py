# -*- coding: utf-8 -*-

from django.db import models


class Directivos(models.Model):
    clave = models.CharField('Clave Directivo', max_length=11, primary_key=True)
    nombre = models.CharField('Nombre', max_length=20)
    apaterno = models.CharField('Apellido Paterno', max_length=20)
    amaterno = models.CharField('Apellido Materno', max_length=20)
    puesto = models.CharField('Puesto', max_length=30)
