# -*- coding: utf-8 -*-

from django import forms

from directivos.models import Directivos
from material.base import Layout, Fieldset, Row, Span8, Span4

class UTDirectivosForm(forms.ModelForm):

    class Meta:
        model = Directivos
        exclude = ()

    layout = Layout(
        Fieldset('', 'clave'),
        ('nombre', 'apaterno', 'amaterno'), 'puesto',
    )