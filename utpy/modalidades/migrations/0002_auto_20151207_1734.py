# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modalidades', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modalidad',
            name='modalidad',
            field=models.CharField(max_length=14, choices=[(b'Escolarizado', b'Escolarizado'), (b'Despresurizado', b'Despresurizado')]),
        ),
    ]
