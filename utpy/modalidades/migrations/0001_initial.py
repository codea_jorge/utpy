# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Modalidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clavemod', models.CharField(max_length=1)),
                ('modalidad', models.CharField(max_length=14, choices=[(b'E', b'Escolarizado'), (b'D', b'Despresurizado')])),
            ],
        ),
    ]
