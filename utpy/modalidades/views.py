# -*- coding: utf-8 -*-

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView
from usuarios.mixins import EmailPasswordMixin, NextURL

from braces.views import LoginRequiredMixin

from .models import Modalidad


class UTModalidadView(LoginRequiredMixin, NextURL, SuccessMessageMixin, CreateView):
    template_name = 'modalidades/new.jade'
    model = Modalidad
    #success_url = reverse_lazy('register_modalidad')
    success_message = 'modalidad %(modalidad)s agregada'
    fields = ('clavemod', 'modalidad')