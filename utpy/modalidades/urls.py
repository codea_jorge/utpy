# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import UTModalidadView

urlpatterns = [
    url(r'^new/$', UTModalidadView.as_view(), name='register_modalidad'),
]
