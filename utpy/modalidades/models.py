# -*- coding: utf-8 -*-

from django.db import models


class Modalidad(models.Model):
    MODALIDADES = (
        ('Escolarizado', 'Escolarizado'),
        ('Despresurizado', 'Despresurizado'),
    )
    clavemod = models.CharField(max_length=1)
    modalidad = models.CharField(max_length=14, choices=MODALIDADES)

    def __unicode__(self, *args, **kwargs):
        return self.clavemod + ' - ' + self.modalidad
