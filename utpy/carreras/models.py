# -*- coding: utf-8 -*-

from django.db import models


class Carrera(models.Model):
    clave_carrera = models.CharField(max_length=7, primary_key=True)
    carrera = models.CharField(max_length=45)
    tipo = models.CharField(max_length=45)

    def __unicode__(self):
        return self.clave_carrera + ' - ' + self.carrera
