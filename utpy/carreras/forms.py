# -*- coding: utf-8 -*-

from django import forms

from carreras.models import Carrera
from material.base import Layout, Fieldset, Row, Span8, Span4

class UTCarrerasForm(forms.ModelForm):

    class Meta:
        model = Carrera
        exclude = ()

    layout = Layout(
        Fieldset('', 'clave_carrera'),
        'carrera', 'tipo',
    )