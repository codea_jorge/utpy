from functools import reduce

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.views.generic import CreateView, UpdateView, ListView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from carreras.forms import UTCarrerasForm
from carreras.models import Carrera


class UTCarrerasCreationView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'carreras/master.jade'
    form_class = UTCarrerasForm
    model = Carrera
    success_url = reverse_lazy('register_carrera')
    success_message = 'Carrera %(carrera)s creada'


class UTCarrerasEditView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Carrera
    fields = '__all__'
    success_url = reverse_lazy('listar_carrera')
    template_name = 'carreras/master.jade'
    success_message = 'Carrera %(clave_carrera)s Actualizada'


class UTCarrerasListView(LoginRequiredMixin, ListView):
    model = Carrera
    template_name = 'carreras/list.jade'
    context_object_name = 'lista_carrera'