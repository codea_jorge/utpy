# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Carrera',
            fields=[
                ('clave_carrera', models.CharField(max_length=7, serialize=False, primary_key=True)),
                ('carrera', models.CharField(max_length=45)),
                ('tipo', models.CharField(max_length=45)),
            ],
        ),
    ]
