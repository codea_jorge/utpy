# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^new/$', UTCarrerasCreationView.as_view(), name='register_carrera'),
    url(r'^edit/(?P<pk>[a-zA-Z-0-9-]+)/$', UTCarrerasEditView.as_view(), name='editar_carrera'),
    url(r'^list/$', UTCarrerasListView.as_view(), name='listar_carrera'),
]
