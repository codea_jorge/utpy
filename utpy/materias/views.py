# -*- coding: utf-8 -*-

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from usuarios.mixins import EmailPasswordMixin, NextURL
from .models import Materia


class UTMateriaView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'materias/new.jade'
    model = Materia

    success_message = 'Materia %(nombre)s agregada'
    fields = '__all__'
    success_url = reverse_lazy('registrar_materia')


class UTMateriaListView(LoginRequiredMixin, ListView):
    model = Materia
    template_name = 'materias/list.jade'
