from django.conf.urls import url
from .views import UTMateriaView, UTMateriaListView

urlpatterns = [
    url(r'^new/$', UTMateriaView.as_view(), name='registrar_materia'),
    url(r'^list/$', UTMateriaListView.as_view(), name='listar_materias'),
]
