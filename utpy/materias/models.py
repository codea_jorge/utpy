# -*- coding: utf-8 -*-

from django.db import models

from carreras.models import Carrera
from modalidades.models import Modalidad


class Materia(models.Model):
    clave_mat = models.CharField(max_length=10, primary_key=True)
    clave_carrera = models.ForeignKey(Carrera)
    plan = models.CharField(max_length=4)
    nombre = models.CharField(max_length=55)
    cuatrimestre = models.CharField(max_length=2)
    clave_mod = models.ForeignKey(Modalidad)

    def __unicode__(self):
        return self.clave_mat + ' - ' + self.nombre
