# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modalidades', '0001_initial'),
        ('carreras', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Materia',
            fields=[
                ('clave_mat', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('plan', models.CharField(max_length=4)),
                ('nombre', models.CharField(max_length=55)),
                ('cuatrimestre', models.CharField(max_length=2)),
                ('clave_carrera', models.ForeignKey(to='carreras.Carrera')),
                ('clave_mod', models.ForeignKey(to='modalidades.Modalidad')),
            ],
        ),
    ]
