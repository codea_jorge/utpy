# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import AbstractUser
from generaciones.models import Generaciones
from profesores.models import Profesores


class Usuarios(AbstractUser):
    matriculaconst = models.ForeignKey(Generaciones, default=None, null=True)
    matriculavar = models.CharField(max_length=5, default=None, null=True)
    claveprof = models.ForeignKey(Profesores, default=None, null=True)
    # is_superuser


# class Administrador(AbstractUser):
#     GENEROS = (
#         ('H', 'Hombre'),
#         ('M', 'Mujer'),
#     )
#     nombre = models.CharField(max_length=30)
#     apellido_p = models.CharField(max_length=30)
#     apellido_m = models.CharField(max_length=30)
#     sexo = models.CharField(max_length=1, choices=GENEROS)
#     fecha_nac = models.DateField()
#     direccion = models.CharField(max_length=50)
