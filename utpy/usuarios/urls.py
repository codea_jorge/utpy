from django.conf.urls import url
from usuarios.views import LoginView, UTUserCreationView
from django.contrib.auth.views import password_reset_confirm, password_reset_complete


urlpatterns = [
    url(r'^new/$', UTUserCreationView.as_view(), name='register_usuario'),
    url(r'^iniciar_sesion/$', LoginView.as_view(), name='iniciar_sesion'),
    url(r'^cerrar_sesion/$', 'django.contrib.auth.views.logout', name='cerrar_sesion'),
    url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', password_reset_confirm, { 'template_name': 'usuarios/password_reset_form.jade'}, name='password_reset'),
    url(r'^reiniciar_contrasena_done/$', password_reset_complete, {'template_name': 'index.jade'}, name='password_reset_complete'),
]
