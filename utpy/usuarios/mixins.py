# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse_lazy


class EmailPasswordMixin(object):
    def send_email_to_set_password(self, request, email,
            token_generator=default_token_generator):
        """
        Send email to set password.
        """
        reset_form = PasswordResetForm({'email': email})

        if reset_form.is_valid():
            # Manda el correo con el enlace de reinicio de contraseņa
            # (siempre y cuando el correo sea valido y el usuario exista en nuestra
            # base de datos)
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                # 'from_email': settings.EMAIL_FROM,
                'email_template_name':
                    'usuarios/password_reset_email.txt',
                'subject_template_name':
                    'usuarios/password_reset_subject.txt',
                'request': request,
                'html_email_template_name':
                    'usuarios/password_reset_email.html',
            }
            reset_form.save(**opts)
        else:
            messages.error(request, 'Correo de cambio de contraseņa no enviado')
#clase para 
class NextURL(object):
    def get_success_url(self):
        #import ipdb;ipdb.set_trace()
        if 'next' in self.request.GET:
            return self.request.GET.get('next')
        return reverse_lazy('index_admin')