# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse_lazy
from django.views.generic import FormView, CreateView
from django.contrib.auth.views import login
from django.core.urlresolvers import reverse_lazy

from braces.views import LoginRequiredMixin

from .forms import UTLoginForm, UTUserCreationForm
from .models import Usuarios
from alumnos.models import Alumnos
from generaciones.models import Generaciones


class LoginView(FormView):
    form_class = UTLoginForm
    # success_url = reverse_lazy('index_admin')
    template_name = 'usuarios/login.jade'

    # Sobreescribir el metodo FormView.form_valid
    def form_valid(self, form):
        usuario = form.get_user()

        login(self.request, usuario)
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        user = self.request.user
        if user.is_superuser:
            return reverse_lazy('index_admin')
        elif user.is_staff and user.claveprof:
            return reverse_lazy('profesor_imparte_list', kwargs={'profesor_id': user.claveprof.claveprof})

        mconst = Generaciones.objects.get(matriculaconst=self.request.user.username[:6])
        alumno = Alumnos.objects.get(matriculaconst=mconst, matriculavar=self.request.user.username[6:])
        return reverse_lazy('alumno_cursos', kwargs={'alumno_id': alumno.id})


class UTUserCreationView(LoginRequiredMixin, CreateView):
    template_name = 'usuarios/new.jade'
    form_class = UTUserCreationForm
    success_url = reverse_lazy('index')
