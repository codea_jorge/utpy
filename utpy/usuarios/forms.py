# -*- coding: utf-8 -*-

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django import forms

from profesores.models import Profesores
from generaciones.models import Generaciones


class UTLoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        help_text ='Alumno: con tu matricula '\
                   'Docente: con clave de docente'
        self.base_fields['username'].help_text = help_text
        super(UTLoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        # It's a profesor?
        try:
            usuario = Profesores.objects.get(claveprof=username)
        except:
            pass

        # Ok, maybe an alumno?
        # First, matriculaconst is valid?
        # try:
        #     matriculaconst_exists = Generaciones.objects.get(
        #         matriculaconst=username[0:6])
        # except:
        #     # raise a validation error cause an invalid matriculaconst
        #     raise forms.ValidationError('Matricula no valida')

        return super(UTLoginForm, self).clean()


# User creation modelform
class UTUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        """ Override this method to hide the username field, cause we want to
        concat the matriculaconst and matriculavar to make the username """
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.HiddenInput()
        self.fields['username'].required = False
