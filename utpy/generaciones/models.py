# -*- coding: utf-8 -*-

from django.db import models
from modalidades.models import Modalidad


class Generaciones(models.Model):
    matriculaconst = models.CharField('Matricula Constatnte',max_length=6)
    generacion = models.CharField(max_length=9)
    modalidad = models.ForeignKey(Modalidad) 

    def __unicode__(self, *args, **kwargs): # reprecentacion 
        return self.matriculaconst + ' - ' + str(self.modalidad)

    class Meta:
        '''creamos una restrinccion para que no permita crear duplicados'''
        unique_together = ('matriculaconst', 'generacion', 'modalidad')
