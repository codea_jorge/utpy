# -*- coding: utf-8 -*-

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views import generic

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from generaciones.models import Generaciones
from usuarios.mixins import NextURL


class UTGeneracionView(SuperuserRequiredMixin, LoginRequiredMixin, NextURL, SuccessMessageMixin, generic.CreateView):
    template_name = 'generaciones/new.jade'
    model = Generaciones
    fields = ('matriculaconst', 'generacion', 'modalidad')

    #success_url = reverse_lazy('registrar_generacion')
    success_message = 'Generacion %(generacion)s creada'