from django.conf.urls import url
from .views import UTGeneracionView

urlpatterns = [
    url(r'^new/$', UTGeneracionView.as_view(), name='registrar_generacion'),
]