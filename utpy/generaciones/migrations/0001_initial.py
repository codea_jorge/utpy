# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modalidades', '0002_auto_20151207_1734'),
    ]

    operations = [
        migrations.CreateModel(
            name='Generaciones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('matriculaconst', models.CharField(max_length=6, verbose_name=b'Matricula Constatnte')),
                ('generacion', models.CharField(max_length=9)),
                ('modalidad', models.ForeignKey(to='modalidades.Modalidad')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='generaciones',
            unique_together=set([('matriculaconst', 'generacion', 'modalidad')]),
        ),
    ]
