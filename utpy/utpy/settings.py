"""
Django settings for utpy project.

Generated by 'django-admin startproject' using Django 1.8.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from . import local

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'w)29)6k*m6s!+z03n2#(u6wv(7eja8ylu(@peu#@t#viuqs1)i'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    #'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'material',
    'pipeline',
    'alumnos',
    'usuarios',
    'generaciones',
    'profesores',
    'modalidades',
    'materias',
    'grupos',
    'inscripciones',
    'documentacion',
    'carreras',
    'calificaciones',
    'directivos',
    'periodos',
    'base',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'utpy.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
         # Django busca los archivos jade en esta variable de conf.
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
         # Tambien logra ubicar archivos jade en dir templates de cada app.
         # Esto es raro, (encuentra jade en apps en la lista de INSTALLED_APPS)
        'APP_DIRS': False, #  Deberia ser True
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            # Esta es una conf de pyjade para que automaticamente convierta los
            # archivos jade a html.
            'loaders': [
                ('pyjade.ext.django.Loader', (
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                ))
            ]
        },
    },
]

WSGI_APPLICATION = 'utpy.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_utpy',
        'USER': 'root',
        'PASSWORD': local.DATABASE_PASSOWORD,
        'HOST': '',
        'Port': '',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static-deploy')

# Variable que indica en que directorio encontrar los archivos estaticos.
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'), )

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
)

# LOGIN y LOGOUT configs
LOGIN_URL = '/usuarios/iniciar_sesion/'
LOGIN_REDIRECT_URL = '/'

# Modelo de autenticacion personalizado
AUTH_USER_MODEL = 'usuarios.Usuarios'

# stylus compiler
PIPELINE_COMPILERS = (
    'pipeline.compilers.stylus.StylusCompiler',
)

PIPELINE_CSS = {
    'main': {
        'source_filenames': (
            'css/main.styl',
        ),
        'output_filename': 'css/main.css',
        'extra_context': {
            'media': 'screen,projection',
        },
    },
}

# Email settings
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = local.EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = local.EMAIL_HOST_PASSWORD
EMAIL_PORT = 587
