# coding: utf-8

from django.conf.urls import include, url
from django.views.generic import TemplateView

from base.views import AdminIndexView
from calificaciones import urls as calificaciones_urls
from usuarios import urls as usuarios_urls
from alumnos import urls as alumnos_urls
from generaciones import urls as generaciones_urls
from profesores import urls as profesores_urls
from directivos import urls as directivos_urls
from modalidades import urls as modalidades_urls
from carreras import urls as carreras_urls
from periodos import urls as periodos_urls
from grupos import urls as grupos_urls
from inscripciones import urls as inscripciones_urls
from materias import urls as materias_urls



urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.jade'), name='index'),
    url(r'^admin$', AdminIndexView.as_view(), name='index_admin'),
    url(r'^usuarios/', include(usuarios_urls)),
    url(r'^profesores/', include(profesores_urls)),
    url(r'^alumnos/', include(alumnos_urls)),
    url(r'^generaciones/', include(generaciones_urls)),
    url(r'^directivos/', include(directivos_urls)),
    url(r'^modalidades/', include(modalidades_urls)),
    url(r'^carreras/', include(carreras_urls)),
    url(r'^periodos/', include(periodos_urls)),
    url(r'^grupos/', include(grupos_urls)),
    url(r'^inscripciones/', include(inscripciones_urls)),
    url(r'^materias/', include(materias_urls)),
    url(r'^calif/', include(calificaciones_urls)),
]