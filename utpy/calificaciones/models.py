# -*- coding: utf-8 -*-

from django.db import models

from alumnos.models import Alumnos
from profesores.models import Imparte
from base.models import Dated


class Calificacion(Dated):
    alumno = models.ForeignKey(Alumnos)
    imparte = models.ForeignKey(Imparte)

    parcial_1 = models.IntegerField(default=0)
    remedial_1 = models.IntegerField(default=0)

    parcial_2 = models.IntegerField(default=0)
    remedial_2 = models.IntegerField(default=0)

    parcial_3 = models.IntegerField(default=0)
    remedial_3 = models.IntegerField(default=0)

    extra = models.IntegerField(default=0)

    class Meta:
        unique_together = ('alumno', 'imparte')

    def __unicode__(self):
        return str(self.alumno) + '000' + str(self.imparte)


class FechasCapturacion(models.Model):
    fecha_ei = models.DateField('Fecha inicio de extra 3')
    fecha_ef = models.DateField('Fecha final de extra 3')

    fecha_r3i = models.DateField('Fecha inicio de remedial 3')
    fecha_r3f = models.DateField('Fecha final de remedial 3')

    fecha_p3i = models.DateField('Fecha inicio de parcial 3')
    fecha_p3f = models.DateField('Fecha final de parcial 3')

    fecha_r2i = models.DateField('Fecha inicio de remedial 2')
    fecha_r2f = models.DateField('Fecha final de remedial 2')

    fecha_p1i = models.DateField('Fecha inicio de parcial 1')
    fecha_p1f = models.DateField('Fecha final de parcial 1')

    fecha_r1i = models.DateField('Fecha inicio de remedial 1')
    fecha_r1f = models.DateField('Fecha final de remedial 1')

    fecha_p2i = models.DateField('Fecha inicio de parcial 2')
    fecha_p2f = models.DateField('Fecha final de parcial 2')

    active = models.BooleanField(default=True)
