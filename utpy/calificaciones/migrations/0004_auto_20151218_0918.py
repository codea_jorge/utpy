# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calificaciones', '0003_auto_20151218_0554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calificacion',
            name='extra',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='parcial_1',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='parcial_2',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='parcial_3',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='remedial_1',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='remedial_2',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='calificacion',
            name='remedial_3',
            field=models.IntegerField(default=0),
        ),
    ]
