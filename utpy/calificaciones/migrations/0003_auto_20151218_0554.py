# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calificaciones', '0002_auto_20151218_0540'),
    ]

    operations = [
        migrations.AddField(
            model_name='calificacion',
            name='created_at',
            field=models.DateTimeField(default=None, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='calificacion',
            name='updated_at',
            field=models.DateTimeField(default=None, auto_now=True),
            preserve_default=False,
        ),
    ]
