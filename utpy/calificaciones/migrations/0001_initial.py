# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profesores', '0003_auto_20151207_1734'),
        ('alumnos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Calificacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parcial_1', models.IntegerField()),
                ('fecha_p1i', models.DateField(verbose_name=b'Fecha inicio de parcial 1')),
                ('fecha_p1f', models.DateField(verbose_name=b'Fecha final de parcial 1')),
                ('remedial_1', models.IntegerField()),
                ('fecha_r1i', models.DateField(verbose_name=b'Fecha inicio de remedial 1')),
                ('fecha_r1f', models.DateField(verbose_name=b'Fecha final de remedial 1')),
                ('parcial_2', models.IntegerField()),
                ('fecha_p2i', models.DateField(verbose_name=b'Fecha inicio de parcial 2')),
                ('fecha_p2f', models.DateField(verbose_name=b'Fecha final de parcial 2')),
                ('remedial_2', models.IntegerField()),
                ('fecha_r2i', models.DateField(verbose_name=b'Fecha inicio de remedial 2')),
                ('fecha_r2f', models.DateField(verbose_name=b'Fecha final de remedial 2')),
                ('parcial_3', models.IntegerField()),
                ('fecha_p3i', models.DateField(verbose_name=b'Fecha inicio de parcial 3')),
                ('fecha_p3f', models.DateField(verbose_name=b'Fecha final de parcial 3')),
                ('remedial_3', models.IntegerField()),
                ('fecha_r3i', models.DateField(verbose_name=b'Fecha inicio de remedial 3')),
                ('fecha_r3f', models.DateField(verbose_name=b'Fecha final de remedial 3')),
                ('extra', models.IntegerField()),
                ('fecha_ei', models.DateField(verbose_name=b'Fecha inicio de extra 3')),
                ('fecha_ef', models.DateField(verbose_name=b'Fecha final de extra 3')),
                ('alumno', models.ForeignKey(to='alumnos.Alumnos')),
                ('imparte', models.ForeignKey(to='profesores.Imparte')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='calificacion',
            unique_together=set([('alumno', 'imparte')]),
        ),
    ]
