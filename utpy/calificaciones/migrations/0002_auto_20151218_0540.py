# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calificaciones', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FechasCapturacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_ei', models.DateField(verbose_name=b'Fecha inicio de extra 3')),
                ('fecha_ef', models.DateField(verbose_name=b'Fecha final de extra 3')),
                ('fecha_r3i', models.DateField(verbose_name=b'Fecha inicio de remedial 3')),
                ('fecha_r3f', models.DateField(verbose_name=b'Fecha final de remedial 3')),
                ('fecha_p3i', models.DateField(verbose_name=b'Fecha inicio de parcial 3')),
                ('fecha_p3f', models.DateField(verbose_name=b'Fecha final de parcial 3')),
                ('fecha_r2i', models.DateField(verbose_name=b'Fecha inicio de remedial 2')),
                ('fecha_r2f', models.DateField(verbose_name=b'Fecha final de remedial 2')),
                ('fecha_p1i', models.DateField(verbose_name=b'Fecha inicio de parcial 1')),
                ('fecha_p1f', models.DateField(verbose_name=b'Fecha final de parcial 1')),
                ('fecha_r1i', models.DateField(verbose_name=b'Fecha inicio de remedial 1')),
                ('fecha_r1f', models.DateField(verbose_name=b'Fecha final de remedial 1')),
                ('fecha_p2i', models.DateField(verbose_name=b'Fecha inicio de parcial 2')),
                ('fecha_p2f', models.DateField(verbose_name=b'Fecha final de parcial 2')),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_ef',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_ei',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p1f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p1i',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p2f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p2i',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p3f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_p3i',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r1f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r1i',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r2f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r2i',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r3f',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='fecha_r3i',
        ),
    ]
