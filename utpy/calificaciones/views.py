# coding: utf-8

from django.views.generic import CreateView, UpdateView
from django.contrib import messages
from django.contrib.auth.views import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseRedirect
from django.utils import timezone

from braces.views import LoginRequiredMixin

from .models import Calificacion, FechasCapturacion
from profesores.models import Imparte
from alumnos.models import Alumnos
#from .forms import CalificacionForm


class UTFechasCapturaEditView(LoginRequiredMixin, UpdateView):
    model = FechasCapturacion
    fields = '__all__'
    template_name = 'calificaciones/fechas_captura.jade'
    success_url = reverse_lazy('index_admin')


class UTFechasCapturaCreateView(LoginRequiredMixin, CreateView):
    model = FechasCapturacion
    fields = '__all__'
    template_name = 'calificaciones/fechas_captura.jade'
    success_url = reverse_lazy('index_admin')


class UTCalificacionCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    success_message = 'Calif added'
    model = Calificacion
    success_url = '/admin/'
    fields = '__all__'
    template_name = 'calificaciones/master.jade'


@login_required
def update_calif(request, curso_id, alumno_id):
    if request.method != 'POST':
        return HttpResponseNotFound('<h1>Page  found</h1>')
    else:
        info = request.POST.copy()
        del info['csrfmiddlewaretoken']

        info['imparte'] = Imparte.objects.get(id=curso_id)
        info['alumno'] = Alumnos.objects.get(id=alumno_id)

        calif, updated = Calificacion.objects.update_or_create(
            imparte=info['imparte'], alumno=info['alumno'], defaults=info.dict())

        if updated:
            messages.success(request, 'Success')

        return redirect('/inscripciones/list/' + curso_id + '/')