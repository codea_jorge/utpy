# coding: utf-8

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^new/$', UTCalificacionCreateView.as_view(), name='agregar_calif'),
    url(r'^fechas_captura/new/$', UTFechasCapturaCreateView.as_view(), name='nueva_fecha_captura'),
    url(r'^fechas_captura/edit/(?P<pk>\d+)$', UTFechasCapturaEditView.as_view(), name='edit_fecha_captura'),
    url(r'^edit/(?P<curso_id>\d+)/(?P<alumno_id>\d+)/$', update_calif, name='edit_calif'),
]