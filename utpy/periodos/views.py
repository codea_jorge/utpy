# -*- coding: utf-8 -*-

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import ListView

from braces.views import LoginRequiredMixin

from periodos.forms import UTPeriodosForm 
from periodos.models import Periodo


class UTPeriodosCreationView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'periodos/new.jade'
    form_class = UTPeriodosForm
    success_url = reverse_lazy('register_periodo')
    success_message = 'Periodo %(periodo)s creado'


class UTPeriodosListView(LoginRequiredMixin, ListView):
    model = Periodo
    template_name = 'periodos/list.jade'
