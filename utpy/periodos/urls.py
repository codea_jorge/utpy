# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import UTPeriodosCreationView, UTPeriodosListView

urlpatterns = [
    url(r'^new/$', UTPeriodosCreationView.as_view(), name='register_periodo'),
    url(r'^list/$', UTPeriodosListView.as_view(), name='listar_periodo'),
]