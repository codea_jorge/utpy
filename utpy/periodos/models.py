# -*- coding: utf-8 -*-

from django.db import models


class Periodo(models.Model):
    clave_periodo = models.CharField(max_length=4)
    periodo = models.CharField(max_length=30)

    def __unicode__(self):
        return self.clave_periodo + ' - ' + self.periodo
