# -*- coding: utf-8 -*-

from django import forms 

from periodos.models import Periodo
from material.base import Layout, Fieldset, Row, Span8, Span4

class UTPeriodosForm(forms.ModelForm):

    class Meta:
        model = Periodo
        exclude = ()

    layout = Layout(
        Fieldset('', 'clave_periodo'),
        'periodo',
    )



