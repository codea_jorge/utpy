# -*- coding: utf-8 -*-

import operator

from functools import reduce

from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from .forms import UTAlumnoForm
from usuarios.models import Usuarios
from usuarios.mixins import EmailPasswordMixin
from alumnos.models import Alumnos
from calificaciones.models import Calificacion


class UTAlumnoCreationView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView): #EmailPasswordMixin,
    template_name = 'alumnos/master.jade'
    form_class = UTAlumnoForm
    model = Alumnos
    success_url = reverse_lazy('register_alumno')
    success_message = 'Alumno %(nombre)s creado'

    def post(self, request, *args, **kwargs):
        resp = super(UTAlumnoCreationView, self).post(request, *args, **kwargs)
        print self.object # retorna el nuevo alumno

        # Create user for new alumno (self.object) crea suario para nuevo alumno
        user = Usuarios.objects.create(
            email=self.object.correo,
            matriculavar=self.object.matriculavar,
            matriculaconst=self.object.matriculaconst,
            username=self.object.matriculaconst.matriculaconst + self.object.matriculavar)

        user.set_password('0000')
        user.save()

        #self.send_email_to_set_password(request, self.object.correo)


        return resp

    def get_context_data(self, **kwargs):
        context = super(UTAlumnoCreationView, self).get_context_data(**kwargs)
        # -created_at indica orden inverso (mas nuevo primero)
        all_alumnos = self.model.objects.all().order_by('-created_at')
        context['lista_alumno'] = all_alumnos[:10]
        context['link_listar_alumno'] = True
        return context


class UTAlumnosEditView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Alumnos
    fields = '__all__'
    success_url = reverse_lazy('listar_alumno')
    template_name = 'alumnos/master.jade'
    success_message = 'Actualizado el alumno %(nombre)s con matricula %(matriculaconst)s %(matriculavar)s'


class UTAlumnoListView(LoginRequiredMixin, ListView):
    model = Alumnos
    template_name = 'alumnos/list.jade'
    context_object_name = 'lista_alumno'
    paginate_by = 5  # No. elementos por pagina

    def get_context_data(self, **kwargs):
        ctx = super(UTAlumnoListView, self).get_context_data(**kwargs)
        ctx['search_url'] = 'listar_alumno'
        return ctx

    def get_queryset(self):
        queryset = super(UTAlumnoListView, self).get_queryset()
        # En el admin_base.jade tenemos un input#search(name='q', type='search')
        # usamos la sig linea para obtener la consulta solicitada.
        q = self.request.GET.getlist('q')
        terms = [term for term in q]
        if q:  # Si el campo no esta vacio, construimos el filtro
            queryset = reduce(operator.or_,
                (Alumnos.objects.filter(Q(matriculavar__contains=t) \
                    | Q(matriculaconst__matriculaconst__contains=t) \
                    | Q(nombre__contains=t) | Q(apellido_p__contains=t) \
                    | Q(apellido_m__contains=t) | Q(correo__contains=t)) \
                    for t in terms
                )
            )
        return queryset


class UTAlumnoDeleView(SuccessMessageMixin, DeleteView):
    model = Alumnos
    fields = '__all__'
    success_url = reverse_lazy('listar_alumno')
    template_name = 'alumnos/elim.jade'
    success_message = '%(nombre)s %(apellido_p)s Eliminado'


class UTAlumnoMaterias(LoginRequiredMixin, ListView):
    model = Calificacion
    template_name = 'alumnos/cursos.jade'

    def get_queryset(self):
        queryset = super(UTAlumnoMaterias, self).get_queryset()
        try:
            alumno = Alumnos.objects.get(id=self.kwargs.get('alumno_id'))
            return queryset.filter(alumno=alumno)
        except:
            pass

        return queryset.none()
