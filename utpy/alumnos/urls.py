# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^new/$', UTAlumnoCreationView.as_view(), name='register_alumno'),
    url(r'^edit/(?P<pk>\d+)/$', UTAlumnosEditView.as_view(), name='editar_alumno'),
    url(r'^list/$', UTAlumnoListView.as_view(), name='listar_alumno'),
    url(r'^(?P<alumno_id>\d+)/cursos/$', UTAlumnoMaterias.as_view(), name='alumno_cursos'),
]
