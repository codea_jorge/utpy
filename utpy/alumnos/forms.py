# -*- coding: utf-8 -*-

from django import forms

from alumnos.models import Alumnos
from material.base import Layout, Fieldset, Row, Span8, Span4
from generaciones.models import Generaciones


class UTAlumnoForm(forms.ModelForm):

    class Meta:
        model = Alumnos
        exclude = ()

    layout = Layout(
        Fieldset('', Row('matriculaconst', 'matriculavar')),
        Row('nombre', 'apellido_p', 'apellido_m'), 'sexo',
        'fechanac', Row(Span8('correo'), Span4('telefono')), 'direccion', 'tiposangre',
    )
