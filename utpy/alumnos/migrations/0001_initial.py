# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumnos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('matriculavar', models.CharField(max_length=5, verbose_name=b'Matricula Variable')),
                ('nombre', models.CharField(max_length=30, verbose_name=b'Nombre')),
                ('apellido_p', models.CharField(max_length=25, verbose_name=b'Apellido Paterno')),
                ('apellido_m', models.CharField(max_length=25, verbose_name=b'Apellido Materno')),
                ('sexo', models.CharField(max_length=1, verbose_name=b'Sexo', choices=[(b'h', b'hombre'), (b'f', b'mujer')])),
                ('fechanac', models.DateField(verbose_name=b'Fcha de Nacimiento')),
                ('direccion', models.CharField(max_length=50, verbose_name=b'Direci\xc3\xb3n')),
                ('correo', models.EmailField(max_length=254, verbose_name=b'Correo Electronico')),
                ('telefono', models.CharField(max_length=15, verbose_name=b'T\xc3\xa9lefono')),
                ('tiposangre', models.CharField(max_length=3, verbose_name=b'Tipo de Sangre', choices=[(b'0+', b'O positivo'), (b'a+', b'A positivo'), (b'b+', b'B positivo'), (b'0-', b'O negatvo'), (b'a-', b'A negatvo'), (b'b-', b'B negatvo')])),
            ],
        ),
    ]
