# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modalidades', '0002_auto_20151207_1734'),
        ('generaciones', '0001_initial'),
        ('alumnos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumnos',
            name='matriculaconst',
            field=models.ForeignKey(to='generaciones.Generaciones'),
        ),
        migrations.AddField(
            model_name='alumnos',
            name='modalidad',
            field=models.ForeignKey(to='modalidades.Modalidad'),
        ),
        migrations.AlterUniqueTogether(
            name='alumnos',
            unique_together=set([('matriculaconst', 'matriculavar', 'modalidad')]),
        ),
    ]
