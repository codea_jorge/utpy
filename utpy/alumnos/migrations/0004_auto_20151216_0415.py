# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alumnos', '0003_auto_20151210_0438'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='alumnos',
            unique_together=set([('matriculaconst', 'matriculavar')]),
        ),
        migrations.RemoveField(
            model_name='alumnos',
            name='modalidad',
        ),
    ]
