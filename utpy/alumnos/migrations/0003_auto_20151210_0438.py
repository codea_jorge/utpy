# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alumnos', '0002_auto_20151208_0605'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumnos',
            name='created_at',
            field=models.DateTimeField(default=None, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='alumnos',
            name='updated_at',
            field=models.DateTimeField(default=None, auto_now=True),
            preserve_default=False,
        ),
    ]
