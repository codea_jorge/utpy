# -*- coding: utf-8 -*-

from django.db import models

from base.models import Dated
from generaciones.models import Generaciones


class Alumnos(Dated):
    GENEROS = (
        ('h', 'hombre'),
        ('f', 'mujer'),
    )
    SANGRE = (
        ('0+', 'O positivo'),
        ('a+', 'A positivo'),
        ('b+', 'B positivo'),
        ('0-', 'O negatvo'),
        ('a-', 'A negatvo'),
        ('b-', 'B negatvo'),
    )

    matriculaconst = models.ForeignKey(Generaciones)
    matriculavar = models.CharField('Matricula Variable',max_length=5)
    nombre = models.CharField('Nombre',max_length=30)
    apellido_p = models.CharField('Apellido Paterno',max_length=25)
    apellido_m = models.CharField('Apellido Materno',max_length=25)
    sexo = models.CharField('Sexo',max_length=1, choices=GENEROS)
    fechanac = models.DateField('Fcha de Nacimiento')
    direccion = models.CharField('Direción',max_length=50)
    correo = models.EmailField('Correo Electronico')
    telefono = models.CharField('Télefono',max_length=15)
    tiposangre = models.CharField('Tipo de Sangre',max_length=3, choices=SANGRE)

    def __unicode__(self):
        return u'{}{} - {}'.format(self.matriculaconst.matriculaconst, self.matriculavar, self.nombre)

    class Meta:
        '''creamos una restrinccion para que no permita crear duplicados'''
        unique_together = ('matriculaconst', 'matriculavar')
