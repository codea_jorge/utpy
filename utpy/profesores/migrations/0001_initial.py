# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profesores',
            fields=[
                ('claveprof', models.CharField(max_length=3, serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=15)),
                ('paterno', models.CharField(max_length=15)),
                ('materno', models.CharField(max_length=15)),
                ('sexo', models.CharField(max_length=1, choices=[(b'H', b'hombre'), (b'M', b'mujer')])),
                ('direccion', models.CharField(max_length=50)),
                ('correo', models.EmailField(max_length=20)),
            ],
        ),
    ]
