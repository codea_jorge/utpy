# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profesores', '0002_imparte'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profesores',
            name='correo',
            field=models.EmailField(max_length=50),
        ),
    ]
