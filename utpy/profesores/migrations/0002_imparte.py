# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('materias', '0001_initial'),
        ('grupos', '0001_initial'),
        ('periodos', '0001_initial'),
        ('profesores', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Imparte',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clave_grupo', models.ForeignKey(to='grupos.Grupo')),
                ('clave_mat', models.ForeignKey(to='materias.Materia')),
                ('clave_periodo', models.ForeignKey(to='periodos.Periodo')),
                ('claveprof', models.ForeignKey(to='profesores.Profesores')),
            ],
        ),
    ]
