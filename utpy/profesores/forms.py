# -*- coding: utf-8 -*-  
from django.forms import ModelForm
from django.forms.widgets import HiddenInput

from .models import Profesores, Imparte


class ProfesoresForm(ModelForm):
    class Meta:
        model = Profesores
        fields = '__all__'


class ProfesorImparteForm(ModelForm):
    class Meta:
        model = Imparte
        fields = '__all__'


class CustomProfesorImparteForm(ProfesorImparteForm):
    class Meta(ProfesorImparteForm.Meta):
        widgets = {
            'claveprof': HiddenInput()
        }
