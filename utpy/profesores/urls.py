# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^new/$', UTProfesorCreateView.as_view(), name='register_profesor'),
    url(r'^edit/(?P<pk>\d+)/$', UTProfesoresEditView.as_view(), name='editar_profesor'),
    url(r'^dele/(?P<pk>\d+)/$', UTProfesoresDeleView.as_view(), name='eliminar_profesor'),
    url(r'^list/$', UTProfesoresListView.as_view(), name='listar_profesor'),
    url(r'^imparte/$', UTProfesorImparteView.as_view(), name='profesor_imparte'),
    url(r'^imparte/(?P<profesor_id>\d+)/$', UTProfesorImparteView.as_view(), name='profesor_imparte'),
    url(r'^imparte/(?P<profesor_id>\d+)/list/$', UTProfesorImparteListView.as_view(), name='profesor_imparte_list'),
]
