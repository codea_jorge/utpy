# -*- coding: utf-8 -*-

from django.db import models

from base.models import Dated
from materias.models import Materia
from grupos.models import Grupo
from periodos.models import Periodo


class Profesores(Dated): #se elimina (models.Model) y solo se deja Dated para poder listar por fechas......
    GENEROS = (
        ('H', 'hombre'),
        ('M', 'mujer'),
    )
    claveprof = models.CharField(max_length=3, primary_key=True)
    nombre = models.CharField(max_length=15)
    paterno = models.CharField(max_length=15)
    materno = models.CharField(max_length=15)
    sexo = models.CharField(max_length=1, choices=GENEROS)
    direccion = models.CharField(max_length=50)
    correo = models.EmailField(max_length=50)

    def __unicode__(self):
        return self.claveprof + ' - ' + self.nombre


class Imparte(models.Model):
    claveprof = models.ForeignKey(Profesores)
    clave_mat = models.ForeignKey(Materia)
    clave_grupo = models.ForeignKey(Grupo)
    clave_periodo = models.ForeignKey(Periodo)

    def __unicode__(self):
        return str(self.claveprof) + ' - ' + str(self.clave_grupo)
