# -*- coding: utf-8 -*-

import operator

from functools import reduce

from django.core.urlresolvers import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from .forms import ProfesorImparteForm, ProfesoresForm, CustomProfesorImparteForm
from .models import Profesores, Imparte
from usuarios.models import Usuarios
from usuarios.mixins import EmailPasswordMixin, NextURL


class UTProfesorCreateView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin,  CreateView): #EmailPasswordMixin,
    template_name = 'profesores/master.jade'
    form_class = ProfesoresForm
    model = Profesores 
    success_url = reverse_lazy('register_profesor')
    success_message = ' %(nombre)s %(paterno)s %(materno)s ha'\
                      ' sido agregado como profesor'
    #fields = '__all__'
    

    def post(self, request, *args, **kwargs):
        resp = super(UTProfesorCreateView, self).post(request, *args, **kwargs)

        data = {
            'username': self.object.claveprof,
            'email': self.object.correo,
            'claveprof': self.object,
            'is_staff':True,#es un nivel del autentificacion de django cuando se cre un usuario profesor crea su 
        }
        user, _ = Usuarios.objects.get_or_create(**data)
        user.set_password('0000')
        user.save()

        #self.send_email_to_set_password(request, self.object.correo)

        return resp

    def get_context_data(self, **kwargs):
        context = super(UTProfesorCreateView, self).get_context_data(**kwargs)
        #ordenamos por fechas y ls 10 ms
        all_profesores = self.model.objects.all().order_by('-created_at')
        context['lista_profesores'] = all_profesores[:10]
        context['link_listar_profesor'] = True
        return context


class UTProfesorImparteView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Imparte
    form_class = ProfesorImparteForm
    success_url = reverse_lazy('listar_profesor')
    success_message = '%(claveprof)s imparte %(clave_mat)s'
    template_name = 'profesores/imparte.jade'

    def get_initial(self):
        # WTF inicialize claveprof con lo que me pasa en la url. Esto es nuevo.
        self.initial['claveprof'] = self.kwargs.get('profesor_id')
        return super(UTProfesorImparteView, self).get_initial()

    def get_form_class(self):
        if self.kwargs.get('profesor_id'):
            return CustomProfesorImparteForm
        return super(UTProfesorImparteView, self).get_form_class()

    def get_context_data(self, **kwargs):
        cxt = super(UTProfesorImparteView, self).get_context_data(**kwargs)
        try:
            profesor = Profesores.objects.get(claveprof=self.kwargs.get('profesor_id'))
            cxt['nombre_completo'] = ' '.join([profesor.nombre, profesor.paterno, profesor.materno])
        except:
            pass
        return cxt

class UTProfesoresEditView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Profesores
    fields = '__all__'
    success_url = reverse_lazy('listar_profesor')
    template_name = 'profesores/master.jade'
    success_message = '%(nombre)s %(paterno)s %(materno)s actualizado'


class UTProfesoresListView(LoginRequiredMixin, ListView):
    model = Profesores 
    template_name = 'profesores/list.jade'
    context_object_name = 'lista_profesores'


class UTProfesoresDeleView(SuperuserRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Profesores
    fields = '__all__'
    success_url = reverse_lazy('listar_profesor')
    template_name = 'profesores/elim.jade'
    success_message = '%(nombre)s %(paterno)s %(materno)s Eliminado'


class UTProfesorImparteListView(LoginRequiredMixin, ListView):
    model = Imparte
    template_name = 'profesores/imparte_list.jade'

    def get_queryset(self):
        queryset = super(UTProfesorImparteListView, self).get_queryset()
        return queryset.filter(claveprof=self.request.user.claveprof)
