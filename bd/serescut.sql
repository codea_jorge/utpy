/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50508
Source Host           : 127.0.0.1:3306
Source Database       : serescut

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2015-12-07 21:58:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administrador
-- ----------------------------
DROP TABLE IF EXISTS `administrador`;
CREATE TABLE `administrador` (
  `id_admin` varchar(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_p` varchar(30) NOT NULL,
  `apellido_m` varchar(30) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fecha_nac` date NOT NULL,
  `direccion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of administrador
-- ----------------------------
INSERT INTO `administrador` VALUES ('111', 'Miguel Angel', 'Sanchez', 'Garcia', 'H', '1982-08-17', 'Chilapa');

-- ----------------------------
-- Table structure for alumnos
-- ----------------------------
DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `modalidad` varchar(1) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_p` varchar(25) NOT NULL,
  `apellido_m` varchar(25) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fechanac` date NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `correo` varchar(20) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `tiposangre` varchar(3) NOT NULL,
  PRIMARY KEY (`matriculaconst`,`matriculavar`),
  KEY `alumnos_modalidad` (`modalidad`),
  CONSTRAINT `fk_alumnos_generacion` FOREIGN KEY (`matriculaconst`) REFERENCES `generaciones` (`matriculaconst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alumnos
-- ----------------------------

-- ----------------------------
-- Table structure for calificaciones
-- ----------------------------
DROP TABLE IF EXISTS `calificaciones`;
CREATE TABLE `calificaciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `modalidad` varchar(1) NOT NULL,
  `claveprof` int(3) NOT NULL,
  `clavemat` varchar(10) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  `parcial1` int(2) DEFAULT NULL,
  `fechap1i` date DEFAULT NULL,
  `fechap1f` date DEFAULT NULL,
  `remedial1` int(2) DEFAULT NULL,
  `fechar1i` date DEFAULT NULL,
  `fechar1f` date DEFAULT NULL,
  `parcial2` int(2) DEFAULT NULL,
  `fechap2i` date DEFAULT NULL,
  `fechap2f` date DEFAULT NULL,
  `remedial2` int(2) NOT NULL,
  `fechar2i` date NOT NULL,
  `fechar2f` date NOT NULL,
  `parcial3` int(2) NOT NULL,
  `fechap3i` date NOT NULL,
  `fechap3f` date NOT NULL,
  `remedial3` int(2) NOT NULL,
  `fechar3i` date NOT NULL,
  `fechar3f` date NOT NULL,
  `extra` int(2) NOT NULL,
  `fechaei` date NOT NULL,
  `fechaef` date NOT NULL,
  UNIQUE KEY `uk_calificacion` (`matriculaconst`,`matriculavar`,`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`,`modalidad`),
  KEY `index_inscripciones` (`matriculaconst`,`matriculavar`,`clavegrupo`,`modalidad`),
  KEY `index_imparte` (`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`),
  CONSTRAINT `calificaciones_imparte` FOREIGN KEY (`claveprof`, `clavemat`, `clavegrupo`, `claveperiodo`) REFERENCES `imparte` (`claveprof`, `clavemat`, `clavegrupo`, `claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `calificaciones_inscripciones` FOREIGN KEY (`matriculaconst`, `matriculavar`, `clavegrupo`, `modalidad`) REFERENCES `inscripciones` (`matriculaconst`, `matriculavar`, `clavegrupo`, `modalidad`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of calificaciones
-- ----------------------------

-- ----------------------------
-- Table structure for carreras
-- ----------------------------
DROP TABLE IF EXISTS `carreras`;
CREATE TABLE `carreras` (
  `clavecarrera` varchar(7) NOT NULL,
  `carrera` varchar(45) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`clavecarrera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carreras
-- ----------------------------

-- ----------------------------
-- Table structure for documentacion
-- ----------------------------
DROP TABLE IF EXISTS `documentacion`;
CREATE TABLE `documentacion` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `acta_nac` varchar(2) DEFAULT NULL,
  `curp` varchar(2) DEFAULT NULL,
  `certificado_bach` varchar(2) DEFAULT NULL,
  `compro_domicilio` varchar(2) DEFAULT NULL,
  `fecha_entrega_doc` date DEFAULT NULL,
  `fecha_regreso_doc` date DEFAULT NULL,
  UNIQUE KEY `uk_documentacion` (`matriculaconst`,`matriculavar`),
  KEY `fk_documentacion` (`matriculaconst`,`matriculavar`),
  CONSTRAINT `fk_documentacion_alumnos` FOREIGN KEY (`matriculaconst`, `matriculavar`) REFERENCES `alumnos` (`matriculaconst`, `matriculavar`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of documentacion
-- ----------------------------

-- ----------------------------
-- Table structure for generaciones
-- ----------------------------
DROP TABLE IF EXISTS `generaciones`;
CREATE TABLE `generaciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `generacion` varchar(9) NOT NULL,
  `modalidad` varchar(1) NOT NULL,
  KEY `fk_generacion_modalidad` (`modalidad`),
  KEY `fk_generacion_alumno` (`matriculaconst`) USING BTREE,
  CONSTRAINT `fk_generacion_modalidad` FOREIGN KEY (`modalidad`) REFERENCES `modalidades` (`clavemod`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of generaciones
-- ----------------------------

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `clavegrupo` varchar(12) NOT NULL,
  `clavecarrera` varchar(7) NOT NULL,
  `cuatrimestre` int(2) NOT NULL,
  `letra` varchar(1) NOT NULL,
  `turno` varchar(1) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  PRIMARY KEY (`clavegrupo`),
  KEY `grupos_carreras` (`clavecarrera`),
  KEY `grupos_periodos` (`claveperiodo`),
  CONSTRAINT `fk_grupos_carreras` FOREIGN KEY (`clavecarrera`) REFERENCES `carreras` (`clavecarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_grupos_periodos` FOREIGN KEY (`claveperiodo`) REFERENCES `periodos` (`claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupos
-- ----------------------------

-- ----------------------------
-- Table structure for imparte
-- ----------------------------
DROP TABLE IF EXISTS `imparte`;
CREATE TABLE `imparte` (
  `claveprof` int(3) NOT NULL,
  `clavemat` varchar(10) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  PRIMARY KEY (`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`),
  KEY `fk_imparte_materias` (`clavemat`),
  KEY `fk_imparte_grupos` (`clavegrupo`),
  KEY `fk_imparte_periodos` (`claveperiodo`),
  CONSTRAINT `fk_imparte_grupos` FOREIGN KEY (`clavegrupo`) REFERENCES `grupos` (`clavegrupo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_materias` FOREIGN KEY (`clavemat`) REFERENCES `materias` (`clavemat`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_periodos` FOREIGN KEY (`claveperiodo`) REFERENCES `periodos` (`claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_profesores` FOREIGN KEY (`claveprof`) REFERENCES `profesores` (`claveprof`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of imparte
-- ----------------------------

-- ----------------------------
-- Table structure for inscripciones
-- ----------------------------
DROP TABLE IF EXISTS `inscripciones`;
CREATE TABLE `inscripciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `modalidad` varchar(1) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `fechainscripcion` date NOT NULL,
  PRIMARY KEY (`matriculavar`,`matriculaconst`,`clavegrupo`,`modalidad`),
  KEY `index_inscripciones1` (`matriculavar`,`matriculaconst`),
  KEY `index_inscripciones2` (`clavegrupo`),
  KEY `inscripciones_alumnos` (`matriculaconst`,`matriculavar`),
  CONSTRAINT `inscripciones_alumnos` FOREIGN KEY (`matriculaconst`, `matriculavar`) REFERENCES `alumnos` (`matriculaconst`, `matriculavar`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inscripciones_grupos` FOREIGN KEY (`clavegrupo`) REFERENCES `grupos` (`clavegrupo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inscripciones
-- ----------------------------

-- ----------------------------
-- Table structure for materias
-- ----------------------------
DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `clavemat` varchar(10) NOT NULL,
  `clavecarrera` varchar(7) NOT NULL,
  `plan` varchar(4) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `cuatrimestre` varchar(2) NOT NULL,
  `clavemod` varchar(1) NOT NULL,
  PRIMARY KEY (`clavemat`),
  KEY `materias_carreras` (`clavecarrera`),
  KEY `materias_modalidades` (`clavemod`),
  CONSTRAINT `materias_carreras` FOREIGN KEY (`clavecarrera`) REFERENCES `carreras` (`clavecarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `materias_modalidades` FOREIGN KEY (`clavemod`) REFERENCES `modalidades` (`clavemod`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of materias
-- ----------------------------

-- ----------------------------
-- Table structure for modalidades
-- ----------------------------
DROP TABLE IF EXISTS `modalidades`;
CREATE TABLE `modalidades` (
  `clavemod` varchar(1) NOT NULL,
  `modalidad` varchar(14) NOT NULL,
  PRIMARY KEY (`clavemod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modalidades
-- ----------------------------

-- ----------------------------
-- Table structure for periodos
-- ----------------------------
DROP TABLE IF EXISTS `periodos`;
CREATE TABLE `periodos` (
  `claveperiodo` varchar(4) NOT NULL,
  `periodo` varchar(30) NOT NULL,
  PRIMARY KEY (`claveperiodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of periodos
-- ----------------------------

-- ----------------------------
-- Table structure for profesores
-- ----------------------------
DROP TABLE IF EXISTS `profesores`;
CREATE TABLE `profesores` (
  `claveprof` int(3) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `paterno` varchar(15) NOT NULL,
  `materno` varchar(15) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `correo` varchar(20) NOT NULL,
  PRIMARY KEY (`claveprof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profesores
-- ----------------------------

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` varchar(11) NOT NULL,
  `password` varchar(15) NOT NULL,
  `estatus` int(1) NOT NULL,
  `tipo_usuario` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
DROP TRIGGER IF EXISTS `usuario_alumno`;
DELIMITER ;;
CREATE TRIGGER `usuario_alumno` AFTER INSERT ON `alumnos` FOR EACH ROW INSERT INTO usuarios VALUES(CONCAT(new.matriculaconst,new.matriculavar),'12345','0','Alumno')
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_alumnos_eliminar`;
DELIMITER ;;
CREATE TRIGGER `usuario_alumnos_eliminar` AFTER DELETE ON `alumnos` FOR EACH ROW DELETE FROM usuarios WHERE id_usuario = CONCAT(old.matriculaconst,old.matriculavar)
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_profesor`;
DELIMITER ;;
CREATE TRIGGER `usuario_profesor` AFTER INSERT ON `profesores` FOR EACH ROW INSERT INTO usuarios VALUES(new.claveprof,'55555','0','profesor')
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_profesor_eliminar`;
DELIMITER ;;
CREATE TRIGGER `usuario_profesor_eliminar` AFTER DELETE ON `profesores` FOR EACH ROW DELETE FROM usuarios WHERE id_usuario=old.claveprof
;;
DELIMITER ;
