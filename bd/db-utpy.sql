/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50508
Source Host           : 127.0.0.1:3306
Source Database       : serescut3

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2015-11-20 10:40:53
*/
create database prueva; 
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administrador
-- ----------------------------
DROP TABLE IF EXISTS `administrador`;
CREATE TABLE `administrador` (
  `id_admin` varchar(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_p` varchar(30) NOT NULL,
  `apellido_m` varchar(30) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fecha_nac` date NOT NULL,
  `direccion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of administrador
-- ----------------------------
INSERT INTO `administrador` VALUES ('111', 'Miguel Angel', 'Sanchez', 'Garcia', 'H', '1982-08-17', 'Chilapa');

-- ----------------------------
-- Table structure for alumnos
-- ----------------------------
DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_p` varchar(25) NOT NULL,
  `apellido_m` varchar(25) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fechanac` date NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `correo` varchar(20) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `tiposangre` varchar(3) NOT NULL,
  PRIMARY KEY (`matriculaconst`,`matriculavar`),
  KEY `matriculaconst` (`matriculaconst`) USING BTREE,
  CONSTRAINT `fk_alumnos_generacion` FOREIGN KEY (`matriculaconst`) REFERENCES `generaciones` (`matriculaconst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alumnos
-- ----------------------------
INSERT INTO `alumnos` VALUES ('571419', '00281', 'Jorge Luis', 'Calleja', 'Alvarado', 'H', '1992-03-10', 'Tixtla', 'jorge@hotmail.com', '5729482042', 'A-');
INSERT INTO `alumnos` VALUES ('571419', '00282', 'Gloria Isabel', 'Calzada', 'Moyao', 'M', '1990-07-18', 'Chilapa', 'gloria@hotmail.com', '57893849', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00283', 'Hermenegildo', 'Diaz', 'Renteria', 'H', '1993-09-11', 'Chilapa', 'osogolo@hotmail.com', '57129453', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00284', 'Ignacio', 'Evangelista', 'Galves', 'H', '1992-05-07', 'Chilapa', 'nacho@hotmail.com', '570040394', 'B-');
INSERT INTO `alumnos` VALUES ('571419', '00285', 'Margarito', 'Tepectzin', 'Bonito', 'H', '1991-06-11', 'Zitlala', 'bonito@hotmail.com', '579918374', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00286', 'Sandra Cristina', 'Gahona', 'Sanchez', 'M', '1983-07-14', 'Atzacoaloya', 'sandra@hotmail.com', '5718394030', 'O-');
INSERT INTO `alumnos` VALUES ('571419', '00287', 'Isabel', 'Garcia', 'Leyva', 'M', '1990-05-07', 'Chilapa', 'isa@hotmail.com', '57891830', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00288', 'Alejandro', 'Guerrero', 'Chavelas', 'H', '1990-05-08', 'Chilapa', 'alex@hotmail.com', '570029591', 'A-');
INSERT INTO `alumnos` VALUES ('571419', '00289', 'Alejandra Isabel', 'Meza', 'Franco', 'M', '1990-01-01', 'Chilapa', 'ale@hotmail.com', '570030010', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00290', 'Fabiola', 'Molina', 'Sotelo', 'M', '1993-06-07', 'Chilapa', 'faby@hotmail.com', '5718374959', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00291', 'Maximina', 'Morales', 'Patricio', 'M', '1994-03-26', 'Chilapa', 'max@hotmail.com', '57121314', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00292', 'Luis Enrique', 'Morales', 'Tomatzin', 'H', '1991-02-12', 'Chilapa', 'kike@gmail.com', '571001023', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00293', 'Aurora', 'Rodriguez', 'Morales', 'M', '1991-01-15', 'Hueyicantenango', 'aurora@hotmail.com', '5728489490', 'B+');
INSERT INTO `alumnos` VALUES ('571419', '00294', 'Marco Antonio', 'Tecolapa', 'Valerio', 'H', '1991-03-06', 'Chilapa', 'marco@hotmail.com', '5719383498', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00296', 'Edgar', 'Torito', 'Casarrubias', 'H', '1992-02-17', 'Zitlala', 'edgar@hotmail.com', '5728494859', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00297', 'Flora', 'Trinidad', 'Tlatempa', 'M', '1992-05-10', 'Zitlala', 'flor@hotmail.com', '5781736748', 'A+');
INSERT INTO `alumnos` VALUES ('571419', '00298', 'Jeronimo', 'Valentin', 'Torrescano', 'H', '1987-09-15', 'chilapa', 'jer.val@hotmail.com', '7471255366', 'O+');
INSERT INTO `alumnos` VALUES ('571419', '00302', 'Rosario', 'Corrales', 'Meneses', 'M', '1991-08-01', 'Atempa', 'rosa@hotmail.com', '5721394839', 'O+');
INSERT INTO `alumnos` VALUES ('571519', '00001', 'Maria', 'Lopez', 'Rodriguez', 'M', '1991-05-14', 'Chilapa', 'mar@hotmail.com', '5714810484', 'O+');
INSERT INTO `alumnos` VALUES ('571519', '00002', 'Mauricio', 'Garcia', 'Hernandez', 'H', '1991-03-07', 'Chilapa', 'mgar@hotmail.com', '5749384948', 'A-');

-- ----------------------------
-- Table structure for calificaciones
-- ----------------------------
DROP TABLE IF EXISTS `calificaciones`;
CREATE TABLE `calificaciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `claveprof` int(3) NOT NULL,
  `clavemat` varchar(10) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  `parcial1` int(2) DEFAULT NULL,
  `fechap1i` date DEFAULT NULL,
  `fechap1f` date DEFAULT NULL,
  `remedial1` int(2) DEFAULT NULL,
  `fechar1i` date DEFAULT NULL,
  `fechar1f` date DEFAULT NULL,
  `parcial2` int(2) DEFAULT NULL,
  `fechap2i` date DEFAULT NULL,
  `fechap2f` date DEFAULT NULL,
  `remedial2` int(2) NOT NULL,
  `fechar2i` date NOT NULL,
  `fechar2f` date NOT NULL,
  `parcial3` int(2) NOT NULL,
  `fechap3i` date NOT NULL,
  `fechap3f` date NOT NULL,
  `remedial3` int(2) NOT NULL,
  `fechar3i` date NOT NULL,
  `fechar3f` date NOT NULL,
  `extra` int(2) NOT NULL,
  `fechaei` date NOT NULL,
  `fechaef` date NOT NULL,
  UNIQUE KEY `uk_calificacion` (`matriculaconst`,`matriculavar`,`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`) USING BTREE,
  KEY `index_inscripciones` (`matriculaconst`,`matriculavar`,`clavegrupo`),
  KEY `index_imparte` (`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`),
  CONSTRAINT `calificaciones_imparte` FOREIGN KEY (`claveprof`, `clavemat`, `clavegrupo`, `claveperiodo`) REFERENCES `imparte` (`claveprof`, `clavemat`, `clavegrupo`, `claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `calificaciones_inscripciones` FOREIGN KEY (`matriculaconst`, `matriculavar`, `clavegrupo`) REFERENCES `inscripciones` (`matriculaconst`, `matriculavar`, `clavegrupo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of calificaciones
-- ----------------------------
INSERT INTO `calificaciones` VALUES ('571419', '00282', '1', '1023E-IT09', '10AV-IT-SD15', 'SD15', '8', null, null, null, null, null, '9', null, null, '0', '0000-00-00', '0000-00-00', '8', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00283', '11', '1024E-IT09', '10AV-IT-SD15', 'SD15', '8', null, null, null, null, null, '9', null, null, '0', '0000-00-00', '0000-00-00', '9', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00284', '11', '1024E-IT09', '10AV-IT-SD15', 'SD15', '9', null, null, null, null, null, '8', null, null, '0', '0000-00-00', '0000-00-00', '8', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00285', '12', '1022E-IT09', '10AV-IT-SD15', 'SD15', '9', null, null, null, null, null, '9', null, null, '0', '0000-00-00', '0000-00-00', '9', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00286', '12', '1022E-IT09', '10AV-IT-SD15', 'SD15', '8', null, null, null, null, null, '8', null, null, '0', '0000-00-00', '0000-00-00', '8', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00287', '14', '1026E-IT09', '10AV-IT-SD15', 'SD15', '9', null, null, null, null, null, '9', null, null, '0', '0000-00-00', '0000-00-00', '9', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');
INSERT INTO `calificaciones` VALUES ('571419', '00288', '14', '1026E-IT09', '10AV-IT-SD15', 'SD15', '9', null, null, null, null, null, '9', null, null, '0', '0000-00-00', '0000-00-00', '9', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00', '0', '0000-00-00', '0000-00-00');

-- ----------------------------
-- Table structure for carreras
-- ----------------------------
DROP TABLE IF EXISTS `carreras`;
CREATE TABLE `carreras` (
  `clavecarrera` varchar(7) NOT NULL,
  `carrera` varchar(45) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`clavecarrera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carreras
-- ----------------------------
INSERT INTO `carreras` VALUES ('ING-DIE', 'Desarrollo e Inovacion Empresarial', 'ING');
INSERT INTO `carreras` VALUES ('ING-FF', 'Financiera y Fiscal', 'ING');
INSERT INTO `carreras` VALUES ('ING-TI', 'Tecnologias de la Informacion', 'ING');
INSERT INTO `carreras` VALUES ('TSU-CON', 'Contaduria', 'TSU');
INSERT INTO `carreras` VALUES ('TSU-DN', 'Desarrollo de Negocios', 'TSU');
INSERT INTO `carreras` VALUES ('TSU-TIC', 'Tecnologias de la Informacion y Comunicacion', 'TSU');

-- ----------------------------
-- Table structure for directivos
-- ----------------------------
DROP TABLE IF EXISTS `directivos`;
CREATE TABLE `directivos` (
  `clave` varchar(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apaterno` varchar(20) NOT NULL,
  `amaterno` varchar(20) NOT NULL,
  `puesto` varchar(30) NOT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of directivos
-- ----------------------------
INSERT INTO `directivos` VALUES ('100', 'Faustino', 'Tecolapa', 'Tixteco', 'subdirector TI');
INSERT INTO `directivos` VALUES ('101', 'Migdalia Annel', 'Garcia', 'Villanueva', 'subdirectora DN');
INSERT INTO `directivos` VALUES ('102', 'Flor Yesenia', 'Garcia', 'Hernandez', 'Subdirectora Conta');
INSERT INTO `directivos` VALUES ('103', 'Jorge Aurelio', 'Gonzalez', 'Balbuena', 'Director');

-- ----------------------------
-- Table structure for documentacion
-- ----------------------------
DROP TABLE IF EXISTS `documentacion`;
CREATE TABLE `documentacion` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `acta_nac` varchar(2) DEFAULT NULL,
  `curp` varchar(2) DEFAULT NULL,
  `certificado_bach` varchar(2) DEFAULT NULL,
  `compro_domicilio` varchar(2) DEFAULT NULL,
  `fecha_entrega_doc` date DEFAULT NULL,
  `fecha_regreso_doc` date DEFAULT NULL,
  UNIQUE KEY `uk_documentacion` (`matriculaconst`,`matriculavar`),
  KEY `fk_documentacion` (`matriculaconst`,`matriculavar`),
  CONSTRAINT `fk_documentacion_alumnos` FOREIGN KEY (`matriculaconst`, `matriculavar`) REFERENCES `alumnos` (`matriculaconst`, `matriculavar`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of documentacion
-- ----------------------------

-- ----------------------------
-- Table structure for generaciones
-- ----------------------------
DROP TABLE IF EXISTS `generaciones`;
CREATE TABLE `generaciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `generacion` varchar(9) NOT NULL,
  `modalidad` varchar(1) NOT NULL,
  KEY `fk_generacion_modalidad` (`modalidad`),
  KEY `fk_generacion_alumno` (`matriculaconst`) USING BTREE,
  CONSTRAINT `fk_generacion_modalidad` FOREIGN KEY (`modalidad`) REFERENCES `modalidades` (`clavemod`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (matriculaconst, modalidad)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of generaciones
-- ----------------------------
INSERT INTO `generaciones` VALUES ('571419', '2014-2016', 'E');
INSERT INTO `generaciones` VALUES ('571419', '2014-2017', 'D');
INSERT INTO `generaciones` VALUES ('571519', '2015-2017', 'E');
INSERT INTO `generaciones` VALUES ('571519', '2015-2018', 'D');

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `clavegrupo` varchar(12) NOT NULL,
  `clavecarrera` varchar(7) NOT NULL,
  `cuatrimestre` int(2) NOT NULL,
  `letra` varchar(1) NOT NULL,
  `turno` varchar(1) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  PRIMARY KEY (`clavegrupo`),
  KEY `grupos_carreras` (`clavecarrera`),
  KEY `grupos_periodos` (`claveperiodo`),
  CONSTRAINT `fk_grupos_carreras` FOREIGN KEY (`clavecarrera`) REFERENCES `carreras` (`clavecarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_grupos_periodos` FOREIGN KEY (`claveperiodo`) REFERENCES `periodos` (`claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('10AV-ID-14', 'ING-DIE', '10', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('10AV-ID-15', 'ING-DIE', '10', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('10AV-IF-14', 'ING-FF', '10', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('10AV-IF-15', 'ING-FF', '10', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('10AV-IT-SD15', 'ING-TI', '10', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('1AD-TT-15', 'TSU-TIC', '1', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('1AD-TT-SD14', 'TSU-TIC', '1', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('1AM-TD-14', 'TSU-DN', '1', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('1AM-TD-15', 'TSU-DN', '1', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('1AM-TT-15', 'TSU-TIC', '1', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('1AM-TT-SD14', 'TSU-TIC', '1', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('1BM-TT-SD14', 'TSU-TIC', '1', 'B', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('2AD-TT-15', 'TSU-TIC', '2', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('2AD-TT-SD14', 'TSU-TIC', '2', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('2AM-TD-14', 'TSU-DN', '2', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('2AM-TD-15', 'TSU-DN', '2', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('2AM-TT-15', 'TSU-TIC', '2', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('2AM-TT-SD14', 'TSU-TIC', '2', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('3AD-TT-15', 'TSU-TIC', '3', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('3AD-TT-SD14', 'TSU-TIC', '3', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('3AM-TD-14', 'TSU-DN', '3', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('3AM-TD-15', 'TSU-DN', '3', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('3AM-TT-15', 'TSU-TIC', '3', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('3AM-TT-SD14', 'TSU-TIC', '3', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('4AD-TT-15', 'TSU-TIC', '4', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('4AD-TT-SD14', 'TSU-TIC', '4', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('4AM-TD-14', 'TSU-DN', '4', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('4AM-TD-15', 'TSU-DN', '4', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('4AM-TT-15', 'TSU-TIC', '4', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('4AM-TT-SD14', 'TSU-TIC', '4', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('5AD-TT-15', 'TSU-TIC', '5', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('5AD-TT-SD14', 'TSU-TIC', '5', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('5AM-TD-14', 'TSU-DN', '5', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('5AM-TD-15', 'TSU-DN', '5', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('5AM-TT-15', 'TSU-TIC', '5', 'A', 'M', 'SD15');
INSERT INTO `grupos` VALUES ('5AM-TT-SD14', 'TSU-TIC', '5', 'A', 'M', 'SD14');
INSERT INTO `grupos` VALUES ('6AD-TT-15', 'TSU-TIC', '6', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('6AD-TT-SD14', 'TSU-TIC', '6', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('7AD-TT-15', 'TSU-TIC', '7', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('7AD-TT-SD14', 'TSU-TIC', '7', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('7AV-ID-14', 'ING-DIE', '7', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('7AV-ID-15', 'ING-DIE', '7', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('7AV-IF-14', 'ING-FF', '7', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('7AV-IF-15', 'ING-FF', '7', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('7AV-IT-15', 'ING-TI', '7', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('7AV-IT-SD14', 'ING-TI', '7', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('8AD-TT-15', 'TSU-TIC', '8', 'A', 'D', 'SD15');
INSERT INTO `grupos` VALUES ('8AD-TT-SD14', 'TSU-TIC', '8', 'A', 'D', 'SD14');
INSERT INTO `grupos` VALUES ('8AV-ID-14', 'ING-DIE', '8', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('8AV-ID-15', 'ING-DIE', '8', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('8AV-IF-14', 'ING-FF', '8', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('8AV-IF-15', 'ING-FF', '8', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('8AV-IT-15', 'ING-TI', '8', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('8AV-IT-AE15', 'ING-TI', '8', 'A', 'V', 'EA15');
INSERT INTO `grupos` VALUES ('8AV-IT-SD14', 'ING-TI', '8', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('9AV-ID-14', 'ING-DIE', '9', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('9AV-ID-15', 'ING-DIE', '9', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('9AV-IF-14', 'ING-FF', '9', 'A', 'V', 'SD14');
INSERT INTO `grupos` VALUES ('9AV-IF-15', 'ING-FF', '9', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('9AV-IT-15', 'ING-TI', '9', 'A', 'V', 'SD15');
INSERT INTO `grupos` VALUES ('9AV-IT-MA15', 'ING-TI', '9', 'A', 'V', 'MA15');
INSERT INTO `grupos` VALUES ('9AV-IT-SD14', 'ING-TI', '9', 'A', 'V', 'SD14');

-- ----------------------------
-- Table structure for imparte
-- ----------------------------
DROP TABLE IF EXISTS `imparte`;
CREATE TABLE `imparte` (
  `claveprof` int(3) NOT NULL,
  `clavemat` varchar(10) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `claveperiodo` varchar(4) NOT NULL,
  PRIMARY KEY (`claveprof`,`clavemat`,`clavegrupo`,`claveperiodo`),
  KEY `fk_imparte_materias` (`clavemat`),
  KEY `fk_imparte_grupos` (`clavegrupo`),
  KEY `fk_imparte_periodos` (`claveperiodo`),
  CONSTRAINT `fk_imparte_grupos` FOREIGN KEY (`clavegrupo`) REFERENCES `grupos` (`clavegrupo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_materias` FOREIGN KEY (`clavemat`) REFERENCES `materias` (`clavemat`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_periodos` FOREIGN KEY (`claveperiodo`) REFERENCES `periodos` (`claveperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_imparte_profesores` FOREIGN KEY (`claveprof`) REFERENCES `profesores` (`claveprof`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of imparte
-- ----------------------------
INSERT INTO `imparte` VALUES ('1', '1023E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('11', '1024E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('12', '1022E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('14', '1026E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('15', '1025E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('22', '1021E-IT09', '10AV-IT-SD15', 'SD15');
INSERT INTO `imparte` VALUES ('25', '1027E-IT09', '10AV-IT-SD15', 'SD15');

-- ----------------------------
-- Table structure for inscripciones
-- ----------------------------
DROP TABLE IF EXISTS `inscripciones`;
CREATE TABLE `inscripciones` (
  `matriculaconst` varchar(6) NOT NULL,
  `matriculavar` varchar(5) NOT NULL,
  `clavegrupo` varchar(12) NOT NULL,
  `fechainscripcion` date NOT NULL,
   
  PRIMARY KEY (`matriculavar`,`matriculaconst`,`clavegrupo`),
  KEY `index_inscripciones1` (`matriculavar`,`matriculaconst`),
  KEY `index_inscripciones2` (`clavegrupo`),
  KEY `inscripciones_alumnos` (`matriculaconst`,`matriculavar`),
  CONSTRAINT `inscripciones_alumnos` FOREIGN KEY (`matriculaconst`, `matriculavar`) REFERENCES `alumnos` (`matriculaconst`, `matriculavar`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inscripciones_grupos` FOREIGN KEY (`clavegrupo`) REFERENCES `grupos` (`clavegrupo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inscripciones
-- ----------------------------
INSERT INTO `inscripciones` VALUES ('571419', '00282', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00283', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00284', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00285', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00286', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00287', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00288', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00289', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00290', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00291', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00292', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00293', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00294', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00296', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00297', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00298', '10AV-IT-SD15', '2015-09-01');
INSERT INTO `inscripciones` VALUES ('571419', '00302', '10AV-IT-SD15', '2015-09-01');

-- ----------------------------
-- Table structure for materias
-- ----------------------------
DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `clavemat` varchar(10) NOT NULL,
  `clavecarrera` varchar(7) NOT NULL,
  `plan` varchar(4) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `cuatrimestre` varchar(2) NOT NULL,
  `clavemod` varchar(1) NOT NULL,
  PRIMARY KEY (`clavemat`),
  KEY `materias_carreras` (`clavecarrera`),
  KEY `materias_modalidades` (`clavemod`),
  CONSTRAINT `materias_carreras` FOREIGN KEY (`clavecarrera`) REFERENCES `carreras` (`clavecarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `materias_modalidades` FOREIGN KEY (`clavemod`) REFERENCES `modalidades` (`clavemod`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of materias
-- ----------------------------
INSERT INTO `materias` VALUES ('1019E-IF09', 'ING-FF', '2009', 'Auditoria fiscal', '10', 'D');
INSERT INTO `materias` VALUES ('101D-TT09', 'TSU-TIC', '2009', 'Desarrollo de habilidades del pensamiento logico', '1', 'D');
INSERT INTO `materias` VALUES ('101E-TC09', 'TSU-CON', '2009', 'Matematicas', '1', 'E');
INSERT INTO `materias` VALUES ('101E-TD09', 'TSU-DN', '2009', 'Matematicas', '1', 'E');
INSERT INTO `materias` VALUES ('101E-TT09', 'TSU-TIC', '2009', 'Desarrollo de habilidades de pensamiento lógico', '1', 'E');
INSERT INTO `materias` VALUES ('1020E-ID09', 'ING-DIE', '2009', 'Ingenieria financiera', '10', 'E');
INSERT INTO `materias` VALUES ('1020E-IF09', 'ING-FF', '2009', 'Seminario de defensa fiscal', '10', 'D');
INSERT INTO `materias` VALUES ('1021E-ID09', 'ING-DIE', '2009', 'Desarrollo empresarial', '10', 'E');
INSERT INTO `materias` VALUES ('1021E-IF09', 'ING-FF', '2009', 'Administracion de costos para la toma de decisiones', '10', 'D');
INSERT INTO `materias` VALUES ('1021E-IT09', 'ING-TI', '2009', 'Modelado de procesos de negocios', '10', 'E');
INSERT INTO `materias` VALUES ('1022E-ID09', 'ING-DIE', '2009', 'Direccion de capital humano II', '10', 'E');
INSERT INTO `materias` VALUES ('1022E-IF09', 'ING-FF', '2009', 'Integradora II', '10', 'D');
INSERT INTO `materias` VALUES ('1022E-IT09', 'ING-TI', '2009', 'Desarrollo de aplicaciones Web', '10', 'E');
INSERT INTO `materias` VALUES ('1023E-ID09', 'ING-DIE', '2009', 'Reingenieria organizacional', '10', 'E');
INSERT INTO `materias` VALUES ('1023E-IF09', 'ING-FF', '2009', 'Optativa II:Practicas de seguridad sicial', '10', 'D');
INSERT INTO `materias` VALUES ('1023E-IT09', 'ING-TI', '2009', 'Seguridad de la información', '10', 'E');
INSERT INTO `materias` VALUES ('1024E-ID09', 'ING-DIE', '2009', 'Integradora II', '10', 'E');
INSERT INTO `materias` VALUES ('1024E-IF09', 'ING-FF', '2009', 'Ingles IX', '10', 'D');
INSERT INTO `materias` VALUES ('1024E-IT09', 'ING-TI', '2009', 'Tópicos selectos de TI', '10', 'E');
INSERT INTO `materias` VALUES ('1025E-ID09', 'ING-DIE', '2009', 'Ingles IV', '10', 'E');
INSERT INTO `materias` VALUES ('1025E-IF09', 'ING-FF', '2009', 'Negociacion empresarial', '10', 'D');
INSERT INTO `materias` VALUES ('1025E-IT09', 'ING-TI', '2009', 'Integradora II', '10', 'E');
INSERT INTO `materias` VALUES ('1026E-ID09', 'ING-DIE', '2009', 'Negociacion empresarial', '10', 'E');
INSERT INTO `materias` VALUES ('1026E-IT09', 'ING-TI', '2009', 'Inglés IV', '10', 'E');
INSERT INTO `materias` VALUES ('1027E-IT09', 'ING-TI', '2009', 'Negociación empresarial', '10', 'E');
INSERT INTO `materias` VALUES ('102E-TC09', 'TSU-CON', '2009', 'Informatica', '1', 'E');
INSERT INTO `materias` VALUES ('102E-TD09', 'TSU-DN', '2009', 'Entorno de la Empresa', '1', 'E');
INSERT INTO `materias` VALUES ('102E-TT09', 'TSU-TIC', '2009', 'Soporte Técnico', '1', 'E');
INSERT INTO `materias` VALUES ('103E-TC09', 'TSU-CON', '2009', 'Derecho civil', '1', 'E');
INSERT INTO `materias` VALUES ('103E-TD09', 'TSU-DN', '2009', 'Administracion', '1', 'E');
INSERT INTO `materias` VALUES ('103E-TT09', 'TSU-TIC', '2009', 'Metodología de la programación', '1', 'E');
INSERT INTO `materias` VALUES ('104E-TC09', 'TSU-CON', '2009', 'Contabilidad basica', '1', 'E');
INSERT INTO `materias` VALUES ('104E-TD09', 'TSU-DN', '2009', 'Informatica para Negocios I', '1', 'E');
INSERT INTO `materias` VALUES ('104E-TT09', 'TSU-TIC', '2009', 'Ofimática', '1', 'E');
INSERT INTO `materias` VALUES ('105E-TC09', 'TSU-CON', '2009', 'Fudamentos de administracion', '1', 'E');
INSERT INTO `materias` VALUES ('105E-TD09', 'TSU-DN', '2009', 'Fudamentos de Mercadotecnia', '1', 'E');
INSERT INTO `materias` VALUES ('105E-TT09', 'TSU-TIC', '2009', 'Fundamentos de redes', '1', 'E');
INSERT INTO `materias` VALUES ('106E-TC09', 'TSU-CON', '2009', 'Ingles I', '1', 'E');
INSERT INTO `materias` VALUES ('106E-TD09', 'TSU-DN', '2009', 'Calidad', '1', 'E');
INSERT INTO `materias` VALUES ('106E-TT09', 'TSU-TIC', '2009', 'Expresión oral y escrita I', '1', 'E');
INSERT INTO `materias` VALUES ('107E-TC09', 'TSU-CON', '2009', 'Expresion oral y escrita', '1', 'E');
INSERT INTO `materias` VALUES ('107E-TD09', 'TSU-DN', '2009', 'Ingles I', '1', 'E');
INSERT INTO `materias` VALUES ('107E-TT09', 'TSU-TIC', '2009', 'Idioma extranjero I', '1', 'E');
INSERT INTO `materias` VALUES ('108E-TC09', 'TSU-CON', '2009', 'Formacion sociocultural', '1', 'E');
INSERT INTO `materias` VALUES ('108E-TD09', 'TSU-DN', '2009', 'Expresion Oral y Escrita I', '1', 'E');
INSERT INTO `materias` VALUES ('108E-TT09', 'TSU-TIC', '2009', 'Formación sociocultural I', '1', 'E');
INSERT INTO `materias` VALUES ('109E-TD09', 'TSU-DN', '2009', 'Formacion Sociocultural I', '1', 'E');
INSERT INTO `materias` VALUES ('209E-TC09', 'TSU-CON', '2009', 'Estadistica', '2', 'E');
INSERT INTO `materias` VALUES ('209E-TT09', 'TSU-TIC', '2009', 'Desarrollo de habilidades de pensamiento matemático', '2', 'E');
INSERT INTO `materias` VALUES ('210E-TC09', 'TSU-CON', '2009', 'Informatica II', '2', 'E');
INSERT INTO `materias` VALUES ('210E-TD09', 'TSU-DN', '2009', 'Estadistica para Negocios', '2', 'E');
INSERT INTO `materias` VALUES ('210E-TT09', 'TSU-TIC', '2009', 'Programación', '2', 'E');
INSERT INTO `materias` VALUES ('211E-TC09', 'TSU-CON', '2009', 'Derecho mercantil', '2', 'E');
INSERT INTO `materias` VALUES ('211E-TD09', 'TSU-DN', '2009', 'Informatica para Negocios II', '2', 'E');
INSERT INTO `materias` VALUES ('211E-TT09', 'TSU-TIC', '2009', 'Base de Datos', '2', 'E');
INSERT INTO `materias` VALUES ('212E-TC09', 'TSU-CON', '2009', 'Contabilidad intermedia', '2', 'E');
INSERT INTO `materias` VALUES ('212E-TD09', 'TSU-DN', '2009', 'Estudio del Consumidor', '2', 'E');
INSERT INTO `materias` VALUES ('212E-TT09', 'TSU-TIC', '2009', 'Introducción al análisis y diseño de sistemas', '2', 'E');
INSERT INTO `materias` VALUES ('213E-TC09', 'TSU-CON', '2009', 'Derecho laboral', '2', 'E');
INSERT INTO `materias` VALUES ('213E-TD09', 'TSU-DN', '2009', 'Compras', '2', 'E');
INSERT INTO `materias` VALUES ('213E-TT09', 'TSU-TIC', '2009', 'Redes de área local', '2', 'E');
INSERT INTO `materias` VALUES ('214E-TC09', 'TSU-CON', '2009', 'Economia', '2', 'E');
INSERT INTO `materias` VALUES ('214E-TD09', 'TSU-DN', '2009', 'Presupuestos', '2', 'E');
INSERT INTO `materias` VALUES ('214E-TT09', 'TSU-TIC', '2009', 'Idioma extranjero II', '2', 'E');
INSERT INTO `materias` VALUES ('215E-TC09', 'TSU-CON', '2009', 'Ingles II', '2', 'E');
INSERT INTO `materias` VALUES ('215E-TD09', 'TSU-DN', '2009', 'Gestion de Ventas', '2', 'E');
INSERT INTO `materias` VALUES ('215E-TT09', 'TSU-TIC', '2009', 'Formación sociocultural II', '2', 'E');
INSERT INTO `materias` VALUES ('216E-TC09', 'TSU-CON', '209', 'Formacion sociocultural', '2', 'E');
INSERT INTO `materias` VALUES ('216E-TD09', 'TSU-DN', '2009', 'Ingles II', '2', 'E');
INSERT INTO `materias` VALUES ('217E-TD09', 'TSU-DN', '2009', 'Formacion Sociocultural II', '2', 'E');
INSERT INTO `materias` VALUES ('316E-TT09', 'TSU-TIC', '2009', 'Desarrollo de aplicaciones  web', '3', 'E');
INSERT INTO `materias` VALUES ('317E-TC09', 'TSU-CON', '2009', 'Matematicas financiera', '3', 'E');
INSERT INTO `materias` VALUES ('317E-TT09', 'TSU-TIC', '2009', 'Desarrollo de Aplicaciones I', '3', 'E');
INSERT INTO `materias` VALUES ('318E-TC09', 'TSU-CON', '2009', 'Contabilidad de sociedades', '3', 'E');
INSERT INTO `materias` VALUES ('318E-TD09', 'TSU-DN', '2009', 'Finanzas', '3', 'E');
INSERT INTO `materias` VALUES ('318E-TT09', 'TSU-TIC', '2009', 'Base de datos II', '3', 'E');
INSERT INTO `materias` VALUES ('319E-TC09', 'TSU-CON', '2009', 'Contabilidad superior', '3', 'E');
INSERT INTO `materias` VALUES ('319E-TD09', 'TSU-DN', '2009', 'Estrategis de Venta', '3', 'E');
INSERT INTO `materias` VALUES ('319E-TT09', 'TSU-TIC', '2009', 'Sistemas Operativos', '3', 'E');
INSERT INTO `materias` VALUES ('320E-TC09', 'TSU-CON', '2009', 'Intruduccion al derecho fiscal', '3', 'E');
INSERT INTO `materias` VALUES ('320E-TD09', 'TSU-DN', '2009', 'Administracion de Almacen', '3', 'E');
INSERT INTO `materias` VALUES ('320E-TT09', 'TSU-TIC', '2009', 'Administración de la función informática', '3', 'E');
INSERT INTO `materias` VALUES ('321E-TC09', 'TSU-CON', '2009', 'Analisis e interpretacion de estados financieros ', '3', 'E');
INSERT INTO `materias` VALUES ('321E-TD09', 'TSU-DN', '2009', 'Integradora I', '3', 'E');
INSERT INTO `materias` VALUES ('321E-TT09', 'TSU-TIC', '2009', 'Integradora  I', '3', 'E');
INSERT INTO `materias` VALUES ('322E-TC09', 'TSU-CON', '2009', 'Calidad', '3', 'E');
INSERT INTO `materias` VALUES ('322E-TD09', 'TSU-DN', '2009', 'Investigacion de Mercados I', '3', 'E');
INSERT INTO `materias` VALUES ('322E-TT09', 'TSU-TIC', '2009', 'Idioma extranjero III', '3', 'E');
INSERT INTO `materias` VALUES ('323E-TC09', 'TSU-CON', '2009', 'Integradora I', '3', 'E');
INSERT INTO `materias` VALUES ('323E-TD09', 'TSU-DN', '2009', 'Mercadotecnia Estrategica', '3', 'E');
INSERT INTO `materias` VALUES ('323E-TT09', 'TSU-TIC', '2009', 'Formación sociocultural III', '3', 'E');
INSERT INTO `materias` VALUES ('324E-TC09', 'TSU-CON', '2009', 'Igles III', '3', 'E');
INSERT INTO `materias` VALUES ('324E-TD09', 'TSU-DN', '2009', 'Cominicacion Integral de Mercadotecnia', '3', 'E');
INSERT INTO `materias` VALUES ('325E-TC09', 'TSU-CON', '2009', 'Formacion sociocultural III', '3', 'E');
INSERT INTO `materias` VALUES ('325E-TD09', 'TSU-DN', '2009', 'Ingles III', '3', 'E');
INSERT INTO `materias` VALUES ('326E-TD09', 'TSU-DN', '2009', 'Formacion Sociocultural III', '3', 'E');
INSERT INTO `materias` VALUES ('424E-TT09', 'TSU-TIC', '2009', 'Desarrollo de Aplicaciones II', '4', 'E');
INSERT INTO `materias` VALUES ('425E-TT09', 'TSU-TIC', '2009', 'Estructura de datos', '4', 'E');
INSERT INTO `materias` VALUES ('426E-TC09', 'TSU-CON', '2009', 'Fudamentos de auditoria', '4', 'E');
INSERT INTO `materias` VALUES ('426E-TT09', 'TSU-TIC', '2009', 'Ingeniería de software I', '4', 'E');
INSERT INTO `materias` VALUES ('427E-TC09', 'TSU-CON', '2009', 'Contabilidad de costos I', '4', 'E');
INSERT INTO `materias` VALUES ('427E-TD09', 'TSU-DN', '2009', 'Plan de Negocios', '4', 'E');
INSERT INTO `materias` VALUES ('427E-TT09', 'TSU-TIC', '2009', 'Administración Base de datos', '4', 'E');
INSERT INTO `materias` VALUES ('428E-TC09', 'TSU-CON', '2009', 'Administracion financiera', '4', 'E');
INSERT INTO `materias` VALUES ('428E-TD09', 'TSU-DN', '2009', 'Investigacion de Mercados II', '4', 'E');
INSERT INTO `materias` VALUES ('428E-TT09', 'TSU-TIC', '2009', 'Idioma extranjero IV', '4', 'E');
INSERT INTO `materias` VALUES ('429E-TC09', 'TSU-CON', '2009', 'Presupuestos', '4', 'E');
INSERT INTO `materias` VALUES ('429E-TD09', 'TSU-DN', '2009', 'Mezcla de Mercadotecnia', '4', 'E');
INSERT INTO `materias` VALUES ('430E-TC09', 'TSU-CON', '2009', 'Contribuciones de personas fisicas', '4', 'E');
INSERT INTO `materias` VALUES ('430E-TD09', 'TSU-DN', '2009', 'Produccion Publicitaria I', '4', 'E');
INSERT INTO `materias` VALUES ('431E-TC09', 'TSU-CON', '2009', 'Comercio exterior', '4', 'E');
INSERT INTO `materias` VALUES ('431E-TD09', 'TSU-DN', '2009', 'Comercio Internacional', '4', 'E');
INSERT INTO `materias` VALUES ('432E-TC09', 'TSU-CON', '2009', 'Ingles IV', '4', 'E');
INSERT INTO `materias` VALUES ('432E-TD09', 'TSU-DN', '2009', 'Ingles IV', '4', 'E');
INSERT INTO `materias` VALUES ('433E-TC09', 'TSU-CON', '2009', 'Formacion sociocultural IV', '4', 'E');
INSERT INTO `materias` VALUES ('433E-TD09', 'TSU-DN', '2009', 'Formacion Sociocultural IV', '4', 'E');
INSERT INTO `materias` VALUES ('530E-TT09', 'TSU-TIC', '2009', 'Desarrollo de Aplicaciones III', '5', 'E');
INSERT INTO `materias` VALUES ('531E-TT09', 'TSU-TIC', '2009', 'Ingeniería de software II', '5', 'E');
INSERT INTO `materias` VALUES ('532E-TT09', 'TSU-TIC', '2009', 'Calidad en el desarrollo de software', '5', 'E');
INSERT INTO `materias` VALUES ('533E-TT09', 'TSU-TIC', '2009', 'Administración de Proyectos', '5', 'E');
INSERT INTO `materias` VALUES ('534E-TC09', 'TSU-CON', '2009', 'Auditoria financiera', '5', 'E');
INSERT INTO `materias` VALUES ('534E-TD09', 'TSU-DN', '2009', 'Planeacion Estrategica de Mercadotecnia', '5', 'E');
INSERT INTO `materias` VALUES ('534E-TT09', 'TSU-TIC', '2009', 'Integradora  II', '5', 'E');
INSERT INTO `materias` VALUES ('535E-TC09', 'TSU-CON', '2009', 'Contabilidad de costos II', '5', 'E');
INSERT INTO `materias` VALUES ('535E-TD09', 'TSU-DN', '2009', 'Plan de Exportacion', '5', 'E');
INSERT INTO `materias` VALUES ('535E-TT09', 'TSU-TIC', '2009', 'Expresión oral y escrita II', '5', 'E');
INSERT INTO `materias` VALUES ('536E-TC09', 'TSU-CON', '2009', 'Contribuciones de personas morales', '5', 'E');
INSERT INTO `materias` VALUES ('536E-TD09', 'TSU-DN', '2009', 'Comercio Electronico', '5', 'E');
INSERT INTO `materias` VALUES ('536E-TT09', 'TSU-TIC', '2009', 'Idioma extranjero V', '5', 'E');
INSERT INTO `materias` VALUES ('537E-TC09', 'TSU-CON', '2009', 'Evaluacion financiera de proyectos', '5', 'E');
INSERT INTO `materias` VALUES ('537E-TD09', 'TSU-DN', '2009', 'Produccion Publicitaria II', '5', 'E');
INSERT INTO `materias` VALUES ('538E-TC09', 'TSU-CON', '2009', 'Sueldos y salarios', '5', 'E');
INSERT INTO `materias` VALUES ('538E-TD09', 'TSU-DN', '2009', 'Realaciones Humanas', '5', 'E');
INSERT INTO `materias` VALUES ('539E-TC09', 'TSU-CON', '2009', 'Integradora II', '5', 'E');
INSERT INTO `materias` VALUES ('539E-TD09', 'TSU-DN', '2009', 'Integradora II', '5', 'E');
INSERT INTO `materias` VALUES ('540E-TC09', 'TSU-CON', '2009', 'Expresion oral y escrita', '5', 'E');
INSERT INTO `materias` VALUES ('540E-TD09', 'TSU-DN', '2009', 'Ingles V', '5', 'E');
INSERT INTO `materias` VALUES ('541E-TD09', 'TSU-DN', '2009', 'Expresion Oral y Escrita', '5', 'E');
INSERT INTO `materias` VALUES ('701E-ID09', 'ING-DIE', '2009', 'Estadistica para negocios', '7', 'E');
INSERT INTO `materias` VALUES ('701E-IF09', 'ING-FF', '2009', 'Estructura financiera', '7', 'D');
INSERT INTO `materias` VALUES ('701E-IT09', 'ING-TI', '2009', 'Matemáticas para TI', '7', 'E');
INSERT INTO `materias` VALUES ('702E-ID09', 'ING-DIE', '2009', 'Economia para los negocios', '7', 'E');
INSERT INTO `materias` VALUES ('702E-IF09', 'ING-FF', '2009', 'Contabilidad especiales', '7', 'D');
INSERT INTO `materias` VALUES ('702E-IT09', 'ING-TI', '2009', 'Ingeniería económica', '7', 'E');
INSERT INTO `materias` VALUES ('703E-ID09', 'ING-DIE', '2009', 'Gestion de compras', '7', 'E');
INSERT INTO `materias` VALUES ('703E-IF09', 'ING-FF', '2009', 'Simulador fiscal de personas fisicas', '7', 'D');
INSERT INTO `materias` VALUES ('703E-IT09', 'ING-TI', '2009', 'Administración de Proyectos de TI I', '7', 'E');
INSERT INTO `materias` VALUES ('704E-ID09', 'ING-DIE', '2009', 'Derecho corporativo', '7', 'E');
INSERT INTO `materias` VALUES ('704E-IF09', 'ING-FF', '2009', 'Ingles IV', '7', 'D');
INSERT INTO `materias` VALUES ('704E-IT09', 'ING-TI', '2009', 'Sistemas de calidad en TI', '7', 'E');
INSERT INTO `materias` VALUES ('705E-ID09', 'ING-DIE', '2009', 'Ingles I', '7', 'E');
INSERT INTO `materias` VALUES ('705E-IF09', 'ING-FF', '2009', 'Administracion de tiempo', '7', 'D');
INSERT INTO `materias` VALUES ('705E-IT09', 'ING-TI', '2009', 'Optativa I', '7', 'E');
INSERT INTO `materias` VALUES ('706E-ID09', 'ING-DIE', '2009', 'Administracion del tiempo', '7', 'E');
INSERT INTO `materias` VALUES ('706E-IT09', 'ING-TI', '2009', 'Inglés I', '7', 'E');
INSERT INTO `materias` VALUES ('707E-IT09', 'ING-TI', '2009', 'Administración del tiempo', '7', 'E');
INSERT INTO `materias` VALUES ('806E-IF09', 'ING-FF', '2009', 'Estructura de capital', '8', 'D');
INSERT INTO `materias` VALUES ('807E-ID09', 'ING-DIE', '2009', 'Estrategias financiera', '8', 'E');
INSERT INTO `materias` VALUES ('807E-IF09', 'ING-FF', '2009', 'Simulador fiscal de personas fisicas', '8', 'D');
INSERT INTO `materias` VALUES ('808E-ID09', 'ING-DIE', '2009', 'Estrategias para nuevos negocios', '8', 'E');
INSERT INTO `materias` VALUES ('808E-IF09', 'ING-FF', '2009', 'Contabilidad gubernamental', '8', 'D');
INSERT INTO `materias` VALUES ('808E-IT09', 'ING-TI', '2009', 'Estadística aplicada', '8', 'E');
INSERT INTO `materias` VALUES ('809E-ID09', 'ING-DIE', '2009', 'Direccion de capital humano I', '8', 'E');
INSERT INTO `materias` VALUES ('809E-IF09', 'ING-FF', '2009', 'Ingles VII', '8', 'D');
INSERT INTO `materias` VALUES ('809E-IT09', 'ING-TI', '2009', 'Administración de proyectos de TI II', '8', 'E');
INSERT INTO `materias` VALUES ('810E-ID09', 'ING-DIE', '2009', 'Optativa I', '8', 'E');
INSERT INTO `materias` VALUES ('810E-IF09', 'ING-FF', '2009', 'Planeacion y organizacion de trabajo', '8', 'D');
INSERT INTO `materias` VALUES ('810E-IT09', 'ING-TI', '2009', 'Base de datos para aplicaciones', '8', 'E');
INSERT INTO `materias` VALUES ('811E-ID09', 'ING-DIE', '2009', 'Ingles II', '8', 'E');
INSERT INTO `materias` VALUES ('811E-IT09', 'ING-TI', '2009', 'Redes convergentes', '8', 'E');
INSERT INTO `materias` VALUES ('812E-ID09', 'ING-DIE', '2009', 'Planeacion y organizacion del trabajo', '8', 'E');
INSERT INTO `materias` VALUES ('812E-IT09', 'ING-TI', '2009', 'Inglés II', '8', 'E');
INSERT INTO `materias` VALUES ('813E-IT09', 'ING-TI', '2009', 'Planeación y organización del trabajo', '8', 'E');
INSERT INTO `materias` VALUES ('911E-IF09', 'ING-FF', '2009', 'Administracion de costos e inventarios', '9', 'D');
INSERT INTO `materias` VALUES ('912E-IF09', 'ING-FF', '2009', 'Simulador fiscal de personas morales si foines de locro', '9', 'D');
INSERT INTO `materias` VALUES ('913E-ID09', 'ING-DIE', '2009', 'Tecnica para la inovacion', '9', 'E');
INSERT INTO `materias` VALUES ('913E-IF09', 'ING-FF', '2009', 'Auditoria gubernamental', '9', 'D');
INSERT INTO `materias` VALUES ('914E-ID09', 'ING-DIE', '2009', 'Sistemas de control administrativo', '9', 'E');
INSERT INTO `materias` VALUES ('914E-IF09', 'ING-FF', '2009', 'Evaluacion financiera', '9', 'D');
INSERT INTO `materias` VALUES ('914E-IT09', 'ING-TI', '2009', 'Auditoría de sistemas de TI', '9', 'E');
INSERT INTO `materias` VALUES ('915E-ID09', 'ING-DIE', '2009', 'Estrategias corporativas de ventas', '9', 'E');
INSERT INTO `materias` VALUES ('915E-IF09', 'ING-FF', '2009', 'Integradora I', '9', 'D');
INSERT INTO `materias` VALUES ('915E-IT09', 'ING-TI', '2009', 'Programación de aplicaciones', '9', 'E');
INSERT INTO `materias` VALUES ('916E-ID09', 'ING-DIE', '2009', 'Integradora I', '9', 'E');
INSERT INTO `materias` VALUES ('916E-IF09', 'ING-FF', '2009', 'Recursos humanos', '9', 'D');
INSERT INTO `materias` VALUES ('916E-IT09', 'ING-TI', '2009', 'Aplicación de las telecomunicaciones', '9', 'E');
INSERT INTO `materias` VALUES ('917E-ID09', 'ING-DIE', '2009', 'Optativa', '9', 'E');
INSERT INTO `materias` VALUES ('917E-IF09', 'ING-FF', '2009', 'Ingles VIII', '9', 'D');
INSERT INTO `materias` VALUES ('917E-IT09', 'ING-TI', '2009', 'Integradora I', '9', 'E');
INSERT INTO `materias` VALUES ('918E-ID09', 'ING-DIE', '2009', 'Ingles III', '9', 'E');
INSERT INTO `materias` VALUES ('918E-IF09', 'ING-FF', '2009', 'Direcciones de equipos de alto rendimiento', '9', 'D');
INSERT INTO `materias` VALUES ('918E-IT09', 'ING-TI', '2009', 'Optativa II', '9', 'E');
INSERT INTO `materias` VALUES ('919E-ID09', 'ING-DIE', '2009', 'Direccion de equipos de alto rendimiento', '9', 'E');
INSERT INTO `materias` VALUES ('919E-IT09', 'ING-TI', '2009', 'Inglés III', '9', 'E');
INSERT INTO `materias` VALUES ('920E-IT09', 'ING-TI', '2009', 'Dirección de equipos de alto rendimiento', '9', 'E');

-- ----------------------------
-- Table structure for modalidades
-- ----------------------------
DROP TABLE IF EXISTS `modalidades`;
CREATE TABLE `modalidades` (
  `clavemod` varchar(1) NOT NULL,
  `modalidad` varchar(14) NOT NULL,
  PRIMARY KEY (`clavemod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modalidades
-- ----------------------------
INSERT INTO `modalidades` VALUES ('D', 'Despresurizado');
INSERT INTO `modalidades` VALUES ('E', 'Escolarizado');

-- ----------------------------
-- Table structure for periodos
-- ----------------------------
DROP TABLE IF EXISTS `periodos`;
CREATE TABLE `periodos` (
  `claveperiodo` varchar(4) NOT NULL,
  `periodo` varchar(30) NOT NULL,
  PRIMARY KEY (`claveperiodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of periodos
-- ----------------------------
INSERT INTO `periodos` VALUES ('EA14', 'Enero-Abril 2014');
INSERT INTO `periodos` VALUES ('EA15', 'Enero-Abril 2015');
INSERT INTO `periodos` VALUES ('EA16', 'Enero-Abril 2016');
INSERT INTO `periodos` VALUES ('EA17', 'Enero-Abril 2017');
INSERT INTO `periodos` VALUES ('EA18', 'Enero-Abril 2018');
INSERT INTO `periodos` VALUES ('EA19', 'Enero-Abril 2019');
INSERT INTO `periodos` VALUES ('EA20', 'Enero-Abril 2020');
INSERT INTO `periodos` VALUES ('MA14', 'Mayo-Agosto 2014');
INSERT INTO `periodos` VALUES ('MA15', 'Mayo-Agosto 2015');
INSERT INTO `periodos` VALUES ('MA16', 'Mayo-Agosto 2016');
INSERT INTO `periodos` VALUES ('MA17', 'Mayo-Agosto 2017');
INSERT INTO `periodos` VALUES ('MA18', 'Mayo-Agosto 2018');
INSERT INTO `periodos` VALUES ('MA19', 'Mayo-Agosto 2019');
INSERT INTO `periodos` VALUES ('MA20', 'Mayo-Agosto 2020');
INSERT INTO `periodos` VALUES ('SD14', 'Septiembre-Diciembre 2014');
INSERT INTO `periodos` VALUES ('SD15', 'Septiembre-Diciembre 2015');
INSERT INTO `periodos` VALUES ('SD16', 'Septienbre-Diciembre 2016');
INSERT INTO `periodos` VALUES ('SD17', 'Septiembre-Diciembre 2017');
INSERT INTO `periodos` VALUES ('SD18', 'Septiembre-Diciembre 2018');
INSERT INTO `periodos` VALUES ('SD19', 'Septiembre-Diciembre 2019');
INSERT INTO `periodos` VALUES ('SD20', 'Septiembre-Diciembre 2020');

-- ----------------------------
-- Table structure for profesores
-- ----------------------------
DROP TABLE IF EXISTS `profesores`;
CREATE TABLE `profesores` (
  `claveprof` int(3) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `paterno` varchar(15) NOT NULL,
  `materno` varchar(15) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `correo` varchar(20) NOT NULL,
  PRIMARY KEY (`claveprof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profesores
-- ----------------------------
INSERT INTO `profesores` VALUES ('1', 'Ivan', 'Andraca', 'Adame', 'H', 'Chilapa', 'ivan@hotmail.com');
INSERT INTO `profesores` VALUES ('2', 'Dioema', 'Andraca', 'Mateos', 'M', 'Chilapa', 'doema@hotmail.com');
INSERT INTO `profesores` VALUES ('3', 'Diana Bertha', 'Aquino', 'Vargas', 'M', 'Chilapa', 'diana@hotmail.com');
INSERT INTO `profesores` VALUES ('4', 'Humberto', 'Carrera', 'Silva', 'H', 'Chilapa', 'humberto@hotmail.com');
INSERT INTO `profesores` VALUES ('5', 'Gerardo', 'Flores', 'Ortega', 'H', 'Chilapa', 'gerar@hotmail.com');
INSERT INTO `profesores` VALUES ('6', 'Erasto', 'Fuentes', 'Castro', 'H', 'Chilapa', 'erasto@hotmail.com');
INSERT INTO `profesores` VALUES ('7', 'Flor Yesenia', 'Garcia', 'Hernandez', 'M', 'Chilapa', 'flor@hotmail.com');
INSERT INTO `profesores` VALUES ('8', 'Migdalia Annel', 'Garcia', 'Villanueva', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('9', 'Jorge Aurelio', 'Gonzalez', 'Balbuena', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('10', 'Anahi', 'Gonzales', 'Yebales', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('11', 'Yurismi', 'Iglesias', 'Jijon', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('12', 'Abel', 'Jeronimo', 'Vargas', 'H', 'Chilapa', 'tareasuarm@gmail.com');
INSERT INTO `profesores` VALUES ('13', 'Antonio', 'Lozano', 'Ortega', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('14', 'José Juan', 'Martinez', 'Toledo', 'H', 'Chilapa', 'toledo@ymail.com');
INSERT INTO `profesores` VALUES ('15', 'Fidencio', 'Meneses', 'Garcia', 'H', 'Atempa', '');
INSERT INTO `profesores` VALUES ('16', 'Aquiles', 'Moctezuma', 'Insaldo', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('17', 'Jorge Luis', 'Pintor', 'Flores', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('18', 'Misael', 'Ramirez', 'Moyao', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('19', 'Brenda', 'Rios', 'Giron', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('20', 'Miguel Angel', 'Sanchez', 'Garcia', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('21', 'Juan Miguel', 'Sanchez', 'Nava', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('22', 'Yesenia', 'Santana', 'Cardoso', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('23', 'Anselmo', 'Tecolapa', 'Crescencio', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('24', 'Fredy Omar', 'Torreblanca', 'Ignacio', 'H', 'Chilpo', '');
INSERT INTO `profesores` VALUES ('25', 'Tereza', 'Nava', 'Alfaro', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('26', 'Fernando', 'Jimon', 'Pochotitlan', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('27', 'Silvia', 'Rivera', 'Avila', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('28', 'Elizabeth', 'Salmeron', 'Nava', 'M', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('29', 'Javier', 'Villalva', 'Moctezuma', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('30', 'Victoro', ' Hugo', 'Lopez', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('31', 'Virgilio', 'Santos', 'Bautista', 'H', 'Chilapa', '');
INSERT INTO `profesores` VALUES ('32', 'Faustino', 'Tecolapa', 'Tixteco', 'H', 'Zitlala', '');
INSERT INTO `profesores` VALUES ('33', 'Ignacio', 'Zapoteco', 'Nava', 'H', 'Chilapa', '');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` varchar(11) NOT NULL,
  `password` varchar(15) NOT NULL,
  `estatus` int(1) NOT NULL,
  `tipo_usuario` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('57141900282', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900283', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900284', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900285', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900286', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900287', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900288', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900289', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900290', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900291', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900292', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900293', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900294', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900296', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900297', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900298', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57141900302', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57151900001', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('57151900002', '12345', '0', 'Alumno');
INSERT INTO `usuarios` VALUES ('1', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('2', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('3', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('4', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('5', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('6', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('7', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('8', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('9', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('10', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('11', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('12', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('13', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('14', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('15', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('16', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('17', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('18', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('19', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('20', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('21', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('22', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('23', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('24', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('25', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('26', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('27', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('28', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('29', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('30', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('31', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('32', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('33', '55555', '0', 'profesor');
INSERT INTO `usuarios` VALUES ('57141900281', '12345', '0', 'Alumno');
DROP TRIGGER IF EXISTS `usuario_alumno`;
DELIMITER ;;
CREATE TRIGGER `usuario_alumno` AFTER INSERT ON `alumnos` FOR EACH ROW INSERT INTO usuarios VALUES(CONCAT(new.matriculaconst,new.matriculavar),'12345','0','Alumno')
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_alumnos_eliminar`;
DELIMITER ;;
CREATE TRIGGER `usuario_alumnos_eliminar` AFTER DELETE ON `alumnos` FOR EACH ROW DELETE FROM usuarios WHERE id_usuario = CONCAT(old.matriculaconst,old.matriculavar)
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_profesor`;
DELIMITER ;;
CREATE TRIGGER `usuario_profesor` AFTER INSERT ON `profesores` FOR EACH ROW INSERT INTO usuarios VALUES(new.claveprof,'55555','0','profesor')
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `usuario_profesor_eliminar`;
DELIMITER ;;
CREATE TRIGGER `usuario_profesor_eliminar` AFTER DELETE ON `profesores` FOR EACH ROW DELETE FROM usuarios WHERE id_usuario=old.claveprof
;;
DELIMITER ;
